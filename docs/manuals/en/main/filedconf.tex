\chapter{Client/File daemon Configuration}
\label{blb:FiledConfChapter}
\label{blb:director:configure:filedaemon}
\index[general]{Configuration!Client/File daemon }
\index[general]{Client/File daemon Configuration }

The Client (or File Daemon) Configuration is one of the simpler ones to
specify. Generally, other than changing the Client name so that error messages
are easily identified, you will not need to modify the default Client
configuration file.

For a general discussion of configuration file and resources including the
data types recognized by \bacula{}, please see the
\bilink{Configuration}{blb:ConfigureChapter} chapter of this manual. The
following Client Resource definitions must be defined:

\begin{bitemize}
\item
   \bilink{Client}{blb:ClientResource2} -- to define what Clients are  to
   be backed up.
\item
   \bilink{Director}{blb:DirectorResource4} -- to  define the Director's
   name and its access password.
\item
   \bilink{Messages}{blb:MessagesChapter} -- to define where error  and
   information messages are to be sent.
\end{bitemize}

\section{The Client Resource}
\label{blb:ClientResource2}
\index[general]{Resource!Client }
\index[general]{Client Resource }

The Client Resource (or FileDaemon) resource defines the name of the Client
(as used by the Director) as well as the port on which the Client listens for
Director connections.

\begin{bdescription}

\item [Client (or FileDaemon)]
   \index[fd]{Client (or FileDaemon)}
   \index[fd]{Directive!Client (or FileDaemon)}
   Start of the Client records.  There must be one and only one Client resource
   in the  configuration file, since it defines the properties of the  current
   client program.

\bwebdoc{FileDaemon:FileDaemon:Name}
\item [Name = \bbracket{name}]
   \index[fd]{Name}
   \index[fd]{Directive!Name}
   The client name that must be used  by the Director when connecting. Generally,
   it is a good idea  to use a name related to the machine so that error messages
   can be easily identified if you have multiple Clients.  This directive is
   required.

\bwebdoc{FileDaemon:FileDaemon:WorkingDirectory}
\item [Working Directory = \bbracket{Directory}]
   \index[fd]{Working Directory}
   \index[fd]{Directive!Working Directory}
   This directive  is mandatory and specifies a directory in which the File
   daemon  may put its status files. This directory should be used only  by
   \bacula{}, but may be shared by other \bacula{} daemons provided the daemon
   names on the \bdirectivename{Name} definition are unique for each daemon. This directive
   is required.

   On Win32 systems, in some circumstances you may need to specify a drive
   letter in the specified working directory path.  Also, please be sure
   that this directory is writable by the SYSTEM user otherwise restores
   may fail (the bootstrap file that is transferred to the File daemon from
   the Director is temporarily put in this directory before being passed
   to the Storage daemon).

\bwebdoc{FileDaemon:FileDaemon:PidDirectory}
\item [Pid Directory = \bbracket{Directory}]
   \index[fd]{Pid Directory}
   \index[fd]{Directive!Pid Directory}
   This directive  is mandatory and specifies a directory in which the File daemon
   may put its process Id file files. The process Id file is used to  shutdown
   \bacula{} and to prevent multiple copies of  \bacula{} from running simultaneously.
   This record is required. Standard shell expansion of the \bbracket{Directory}  is
   done when the configuration file is read so that values such  as \textbf{\$HOME}
   will be properly expanded.

   Typically on Linux systems, you will set this to:  \bdirectoryname{/var/run}. If you are
   not installing \bacula{} in the  system directories, you can use the
   \bdirectivename{Working Directory} as  defined above.

\bwebdoc{FileDaemon:FileDaemon:DedupIndexDirectory}
\item [Dedup Index Directory = \bbracket{Directory}]
   \index[fd]{Dedup Index Directory}
   \index[fd]{Directive!Dedup Index Directory}
   This directive is deprecated and replaced by ``\bdirectivename{Client Rehydration}''.
   \smallskip{}
   
   This directive is optional and specifies a directory in which the File Daemon
   may put a index cache of all blocks read during a Backup job. 

   Standard shell expansion of the \bbracket{Directory} is done when the
   configuration file is read so that values such as \textbf{\$HOME} will be
   properly expanded.

\bwebdoc{FileDaemon:FileDaemon:ClientRehydration}
\item [ClientRehydration = \bbracket{Directory}]
   \index[fd]{Client Rehydration}
   \index[fd]{Directive!Client Rehydration}

   This directive is optional and allows to try to do rehydration using
   existing local data on the Client at restore time. In some cases, the use of
   this directive permits to transfer less data over the network during a
   restore. The default value is \bdefaultvalue{no}.


\bwebdoc{FileDaemon:FileDaemon:HeartbeatInterval}
\item [Heartbeat Interval = \bbracket{time-interval}]
   \index[fd]{Heartbeat Interval}
   \index[fd]{Directive!Heartbeat Interval}
   \index[general]{Heartbeat Interval}
   \index[general]{Broken pipe}
   \index[general]{slow}
   \index[general]{Backups!slow}
   This record defines an interval of time in seconds.  For each heartbeat that the
   File daemon receives from the Storage daemon, it will forward it to the
   Director.  In addition, if no heartbeat has been received from the
   Storage daemon and thus forwarded the File daemon will send a heartbeat
   signal to the Director and to the Storage daemon to keep the channels
   active.  The default interval is \bdefaultvalue{300s}.
   This feature is particularly useful if you have a router such as 3Com
   that does not follow Internet standards and times out a valid
   connection after a short duration despite the fact that keepalive is
   set. This usually results in a broken pipe error message.

   If you continue getting broken pipe error messages despite using the
   Heartbeat Interval, and you are using Windows, you should consider
   upgrading your ethernet driver.  This is a known problem with NVidia
   NForce 3 drivers (4.4.2 17/05/2004), or try the following workaround
   suggested by Thomas Simmons for Win32 machines:

   Browse to:
   Start \textrightarrow{} Control Panel \textrightarrow{} Network Connections

   Right click the connection for the nvidia adapter and select properties.
   Under the General tab, click ``Configure\ldots''. Under the Advanced tab set
   ``Checksum Offload'' to disabled and click OK to save the change.

   Lack of communications, or communications that get interrupted can
   also be caused by Linux firewalls where you have a rule that throttles
   connections or traffic.


\bwebdoc{FileDaemon:FileDaemon:MaximumConcurrentJobs}
\item [Maximum Concurrent Jobs = \bbracket{number}]
   \index[fd]{Maximum Concurrent Jobs}
   \index[fd]{Directive!Maximum Concurrent Jobs}
   where \bbracket{number} is the maximum number of Jobs that should run
   concurrently.  The default is set to \bdefaultvalue{20}, but you may set it to a larger
   number.  Each contact from the Director (e.g.  status request, job start
   request) is considered as a Job, so if you want to be able to do a
   \bcommandname{status} request in the console at the same time as a Job is running, you
   will need to set this value greater than \bvalue{1}. If set to a large value,
   please be careful to have this value higher than the \bdirectivename{Maximum Concurrent Jobs}
   configured in the \bresourcename{Client} resource in the Director configuration file.
   Otherwise, backup jobs can fail due to the Director connection to FD be
   refused because \acl{MCJ} was exceeded on FD side.

\item [Maximum Job Error Count = \bbracket{number}]
   \index[fd]{Maximum Job Error Count}
   \index[fd]{Directive!Maximum Job Error Count}
   where \bbracket{number} is the error threshold for the Job, after reaching it Job will be failed.
   The default value is 1000.
   If this value is set to 0, job will continue to run no matter how many errors it encounters.

\bwebdoc{FileDaemon:FileDaemon:FdAddresses}
\item [FDAddresses = \bbracket{IP-address-specification}]
   \index[fd]{FDAddresses}
   \index[fd]{Directive!FDAddresses}
   Specify the ports and addresses on which the File daemon listens for
   Director connections.  Probably the simplest way to explain is to show
   an example:

\begin{bVerbatim}
 FDAddresses  = {
    ip = { addr = 1.2.3.4; port = 1205; }
    ipv4 = {
        addr = 1.2.3.4; port = http; }
    ipv6 = {
        addr = 1.2.3.4;
        port = 1205;
    }
    ip = {
        addr = 1.2.3.4
        port = 1205
    }
    ip = { addr = 1.2.3.4 }
    ip = {
        addr = 201:220:222::2
    }
    ip = {
        addr = bluedot.thun.net
    }
 }
\end{bVerbatim}

where ip, ip4, ip6, addr, and port are all keywords. Note, that  the address
can be specified as either a dotted quadruple, or  IPv6 colon notation, or as
a symbolic name (only in the ip specification).  Also, port can be specified
as a number or as the mnemonic value from  the \bfilename{/etc/services} file.  If a port
is not specified, the default will be used. If an ip  section is specified,
the resolution can be made either by IPv4 or  IPv6. If ip4 is specified, then
only IPv4 resolutions will be permitted,  and likewise with ip6.

\bwebdoc{FileDaemon:FileDaemon:FdPort}
\item [FDPort = \bbracket{port-number}]
   \index[fd]{FDPort}
   \index[fd]{Directive!FDPort}
   This specifies the port number  on which the Client listens for Director
   connections. It must agree  with the FDPort specified in the Client resource
   of the Director's  configuration file. The default is \bdefaultvalue{9102}.

\bwebdoc{FileDaemon:FileDaemon:FdAddress}
\item [FDAddress = \bbracket{IP-Address}]
   \index[fd]{FDAddress}
   \index[fd]{Directive!FDAddress}
   This record is optional,  and if it is specified, it will cause the File
   daemon server (for  Director connections) to bind to the specified
   \textbf{IP-Address},  which is either a domain name or an IP address specified as a
   dotted quadruple. If this record is not specified, the File daemon  will bind
   to any available address (the default).

\bwebdoc{FileDaemon:FileDaemon:FdSourceAddress}
\item [FDSourceAddress = \bbracket{IP-Address}]
   \index[fd]{FDSourceAddress}
   \index[fd]{Directive!FDSourceAddress}
   This record is optional,  and if it is specified, it will cause the File
   daemon server (for  Storage connections) to bind to the specified
   \textbf{IP-Address},  which is either a domain name or an IP address specified as a
   dotted quadruple. If this record is not specified, the kernel will choose
   the best address according to the routing table (the default).

\bwebdoc{FileDaemon:FileDaemon:SdConnectTimeout}
\item [SDConnectTimeout = \bbracket{time-interval}]
   \index[fd]{SDConnectTimeout}
   \index[fd]{Directive!SDConnectTimeout}
   This  record defines an interval of time that  the File daemon will try to
   connect to the  Storage daemon. The default is \bdefaultvalue{30 minutes}. If no connection
   is made in the specified time interval, the File daemon  cancels the Job.

\bwebdoc{FileDaemon:FileDaemon:MaximumNetworkBufferSize}
\item [Maximum Network Buffer Size = \bbracket{bytes}]
   \index[fd]{Maximum Network Buffer Size}
   \index[fd]{Directive!Maximum Network Buffer Size}
   where \bbracket{bytes} specifies the initial network buffer  size to use with
   the File daemon. This size will be adjusted down  if it is too large until it
   is accepted by the OS. Please use  care in setting this value since if it is
   too large, it will  be trimmed by 512 bytes until the OS is happy, which may
   require  a large number of system calls. The default value is \bdefaultvalue{65,536} bytes.
   The maximum value is \bdefaultvalue{1,000,000} bytes.

   Note, on certain Windows machines, there are reports that the
   transfer rates are very slow and this seems to be related to
   the default \bdefaultvalue{65,536} size. On systems where the transfer rates
   seem abnormally slow compared to other systems, you might try
   setting the Maximum Network Buffer Size to 32,768 in both the
   File daemon and in the Storage daemon.

\bwebdoc{FileDaemon:FileDaemon:MaximumBandwidthPerJob}
\item [Maximum Bandwidth Per Job = \bbracket{speed}]
\index[fd]{Maximum Bandwidth Per Job}
\index[fd]{Directive!Maximum Bandwidth Per Job}

The speed parameter specifies the maximum allowed bandwidth in bytes per
second that a job may use. 
You may specify the following speed parameter modifiers:
   \bvalue{kb/s} (1,000 bytes per second), \bvalue{k/s} (1,024 bytes per second),
   \bvalue{mb/s} (1,000,000 bytes per second), or \bvalue{m/s} (1,048,576 bytes per second).

The use of TLS, TLS PSK, CommLine compression and Deduplication can interfer
with the value set with the Directive.

\bwebdoc{FileDaemon:FileDaemon:CommCompression}
\item[CommCompression = \byesorno{}]
\index[fd]{CommCompression}
\index[fd]{Directive!CommCompression}
\index[fd]{Compression}

If the two \bacula{} components (DIR, FD, SD, bconsole) have the comm line
compression enabled, the line compression will be enabled.  The default value
is yes.

In many cases, the volume of data transmitted across the communications line
can be reduced by a factor of three when this directive is
\bdefaultvalue{enabled}. In the case that the compression is not effective,
\bacula{} turns it off on a record by record basis.  \smallskip

If you are backing up data that is already compressed the comm line compression
will not be effective, and you are likely to end up with an average compression
ratio that is very small.  In this case, \bacula{} reports \bvalue{None} in the
Job report.

\bwebdoc{FileDaemon:FileDaemon:DisableCommand}
\item [DisableCommand = \bbracket{cmd}]
The \bdirectivename{Disable Command} adds security to your File daemon by
disabling certain commands globally.  The commands that can be
disabled are:

\begin{bVerbatim}
backup
cancel
setdebug=
setbandwidth=
estimate
fileset
JobId=
level =
restore
endrestore
session
status
.status
storage
verify
RunBeforeNow
RunBeforeJob
RunAfterJob
Run
accurate
\end{bVerbatim}

One or more of these command keywords can be placed in quotes and separated
by spaces on the \bdirectivename{Disable Command} directive line.  Note: the commands must
be written exactly as they appear above.

\bwebdoc{FileDaemon:FileDaemon:SDPacketCheck}
 \item[SD Packet Check = \bbracket{num-packets}]
\index[dir]{SDPacketCheck}
\index[dir]{Directive!SDPacketCheck}

The \bdirectivename{SDPacketCheck} takes a positive integer. The integer if
zero turns off the feature.  If the integer is greater than zero, it is the
number of packets that the FileDaemon will send to the Storage Daemon before to
send a POLL request and wait for the Storage Daemon answer. The default value
is 0. If the time between two POLL requests is too short (less than few
seconds) and the number of bytes transfered is less than few hundred of MB, the
value of the \bdirectivename{SDPacketCheck} is increased dynamically.

\input{FileDaemonFileDaemonTls}

\bwebdoc{FileDaemon:FileDaemon:PkiCipher}
\item[PKI Cipher] 

  This directive is optional and if specified will configure the data
  encryption to use a specific cipher. The default cipher is \bdefaultvalue{AES 128 CBC}.

  \smallskip{}

  The following ciphers are available: aes128, aes192, aes256 and blowfish

\bwebdoc{FileDaemon:FileDaemon:PkiDigest}
\item[PKI Digest] 

  This directive is optional and if specified will configure the data
  encryption to use a specific digest algorithm. The default cipher is \bdefaultvalue{SHA1} or
  \bdefaultvalue{SHA256} depending on the version of OpenSSL.

  \smallskip{}

  The following digest are available: md5, sha1, sha256.

\item [PKI Encryption]
  See the \bilink{Data Encryption}{blb:DataEncryption} chapter of this manual.

\item [PKI Signatures]
  See the \bilink{Data Encryption}{blb:DataEncryption} chapter of this manual.

\item [PKI Keypair]
  See the \bilink{Data Encryption}{blb:DataEncryption} chapter of this manual.

\item [PKI Master Key]
  See the \bilink{Data Encryption}{blb:DataEncryption} chapter of this manual.

\end{bdescription}

The following is an example of a valid Client resource definition:

\begin{bVerbatim}
Client {                              # this is me
  Name = rufus-fd
  WorkingDirectory = $HOME/bacula/bin/working
  Pid Directory = $HOME/bacula/bin/working
}
\end{bVerbatim}

\section{The Director Resource}
\label{blb:DirectorResource4}
\index[general]{Director Resource }
\index[general]{Resource!Director }

The Director resource defines the name and password of the Directors that are
permitted to contact this Client.

\begin{bdescription}

\item [Director]
   \index[fd]{Director}
   \index[fd]{Directive!Director}
   Start of the Director records. There may be any  number of Director resources
   in the Client configuration file. Each  one specifies a Director that is
   allowed to connect to this  Client.

\bwebdoc{FileDaemon:Director:Name}
\item [Name = \bbracket{name}]
   \index[fd]{Name}
   \index[fd]{Directive!Name}
   The name of the Director  that may contact this Client. This name must be the
   same as the name specified on the Director resource  in the Director's
   configuration file. Note, the case (upper/lower) of the characters in
   the name are significant (i.e. S is not the same as s). This directive
   is required.

\bwebdoc{FileDaemon:Director:Password}
\item [Password = \bbracket{password}]
   \index[fd]{Password}
   \index[fd]{Directive!Password}
   Specifies the password that must be  supplied for a Director to be authorized.
This password  must be the same as the password specified in the  Client
resource in the Director's configuration file.  This directive is required.

\bwebdoc{FileDaemon:Director:DirPort}
\item [DirPort = \bbracket{number}]
   \index[fd]{DirPort}
   \index[fd]{Directive!DirPort}

Specify the port to use to connect to the Director. This port must be
identical to the \bdirectivename{DIRport} specified in the \bdirectivename{Director}
resource of the \bilink{Director's configuration}{blb:DirectorChapter} file.
The default is \bdefaultvalue{9101} so this directive is not normally specified.

\bwebdoc{FileDaemon:Director:Address}
\item [Address = \bbracket{address}]
  \index[fd]{Directive!Address}

  Where the address is a host name, a fully qualified domain name, or a network
  address used to connect to the Director. This directive is required when
  \bdirectivename{ConnectToDirector} is enabled.

\bwebdoc{FileDaemon:Director:ConnectToDirector}
\item [ConnectToDirector = \byesorno{}]
  \index[fd]{Directive!ConnectToDirector}
\label{blb:ConnectToDirector}

When the \bdirectivename{ConnectToDirector} directive is set to \texttt{true},
the Client will contact the Director according to the \bresourcename{Schedule}
rules. The connection initiated by the Client will be then used by the Director
to start jobs or issue bconsole commands. If the \bdirectivename{Schedule}
directive is not set, the connection will be initiated when the file daemon
starts. The connection will be reinitialized every
\bdirectivename{ReconnectionTime}. This directive can be useful if your File
Daemon is behind a firewall that permits outgoing connections but not incoming
connections.

\bwebdoc{FileDaemon:Director:ReconnectionTime}
\item [ReconnectionTime = \bbracket{time}]
  \index[fd]{Directive!ReconnectionTime}

  When the Director resource of the FileDaemon is configured to connect the
  Director with the \bdirectivename{ConnectToDirector} directive, the
  connection initiated by the FileDeamon to the Director will be reinitialized
  at a regular interval specified by the \bdirectivename{ReconnectionTime}
  directive. The default value is \bdefaultvalue{40 mins}.

\bwebdoc{FileDaemon:Director:Schedule}
\item [Schedule = \bbracket{sched-resource}]
  \index[fd]{Directive!Schedule}

  The \bdirectivename{Schedule} directive defines what schedule is to be used
  for Client to connect the Director if the directive
  \bdirectivename{ConnectToDirector} is set to \texttt{true}.

  This directive is optional, and if left out, the Client will initiate a
  connection automatically at the start of the daemon. Although you may specify
  only a single Schedule resource for any \bresourcename{Director} resource,
  the \bresourcename{Schedule} resource may contain multiple
  \bdirectivename{Connect} directives, which allow you to initiate the Client
  connection at many different times, and each \bdirectivename{Connect} directive
  permits to set the the \bdirectivename{Max Connect Time} directive.

\bwebdoc{FileDaemon:Director:MaximumBandwidthPerJob}
\item [Maximum Bandwidth Per Job = \bbracket{speed}]
\index[fd]{Maximum Bandwidth Per Job}
\index[fd]{Directive!Maximum Bandwidth Per Job}

The speed parameter specifies the maximum allowed bandwidth in bytes per
second that a job may use when started from this Director.
You may specify the following speed parameter modifiers:
   kb/s (1,000 bytes per second), k/s (1,024 bytes per second),
   mb/s (1,000,000 bytes per second), or m/s (1,048,576 bytes per second).

\bwebdoc{FileDaemon:Director:DisableCommand}
\item [DisableCommand = \bbracket{cmd}]
The \bdirectivename{Disable Command} adds security to your File daemon by
disabling certain commands for the current Director. More information
about the syntax can be found on \vref{FileDaemon:FileDaemon:DisableCommand}.

\bwebdoc{FileDaemon:Director:Monitor}
\item [Monitor = \byesorno{}]
   \index[fd]{Monitor}
   \index[fd]{Directive!Monitor}
   If Monitor is set to \bdefaultvalue{no} (default),  this director will have full access
   to this Client. If Monitor is set to  \bvalue{yes}, this director will only be
   able to fetch the current status  of this Client.

   Please note that if this director is being used by a Monitor, we highly
   recommend to set this directive to \bvalue{yes} to avoid serious security
   problems.

\input{FileDaemonDirectorTls}

\end{bdescription}

Thus multiple Directors may be authorized to use this Client's services. Each
Director will have a different name, and normally a different password as
well.

The following is an example of a valid Director resource definition:

\begin{bVerbatim}
#
# List Directors who are permitted to contact the File daemon
#
Director {
  Name = HeadMan
  Password = very_good                # password HeadMan must supply
}
Director {
  Name = Worker
  Password = not_as_good
  Monitor = Yes
}
\end{bVerbatim}


\label{blb:FDScheduleResource}
\section{The Schedule Resource}
\index[general]{Resource!Schedule}
\index[general]{Schedule Resource}

The Schedule resource provides a means of automatically scheduling the
connection of the Client to the Director. Each Director resource can have
a different Schedule resource.

\begin{bdescription}

\item [Schedule]
\index[fd]{Schedule}
\index[fd]{Directive!Schedule}
   Start of the Schedule directives.  No \bresourcename{Schedule} resource is
   required. If no \bresourcename{Schedule} is used, the Client will connect
   automatically to the Director at the startup.

   \bwebdoc{FileDaemon:Schedule:Name}
\item [Name = \bbracket{name}]
   \index[fd]{Name}
   \index[fd]{Directive!Name}
   The name of the schedule being defined.  The Name directive is required.

   \bwebdoc{FileDaemon:Schedule:Enabled}
  \item [Enabled = \byesorno{}]
  \index[fd]{Enabled}
  \index[fd]{Directive!Enabled}
  This directive allows you to enable or disable the \bresourcename{Schedule} resource.

  \bwebdoc{FileDaemon:Schedule:Connect}
\item [Connect = \bbracket{Connect-overrides} \bbracket{Date-time-specification}]
   \index[fd]{Connect}
   \index[fd]{Directive!Connect}

   The Connect directive defines when a Client should connect to a Director.
   You may specify multiple \bdirectivename{Connect} directives within a
   \bresourcename{Schedule} resource.  If you do, they will all be applied
   (i.e.  multiple schedules).  If you have two \bdirectivename{Connect}
   directives that start at the same time, two connections will start at the
   same time (well, within one second of each other). It is not recommended to
   have multiple connections at the same time.

   \textbf{Connect-options} are specified as: \textbf{keyword=value} where the
   keyword is \bkeyword{MaxConnectTime} and the \bvalue{value} is as defined on the respective
   directive formats for the Job resource.  You may specify multiple \textbf{Connect-options}
   on one \bdirectivename{Connect} directive by separating them with one or
   more spaces or by separating them with a trailing comma.  For example:

\begin{bdescription}

\item [MaxConnectTime=\bbracket{time-spec}]
   \index[fd]{MaxConnectTime}
   \index[fd]{Directive!MaxConnectTime}
   specifies how much time the connection will be attempt and active.

 \end{bdescription}
\end{bdescription}

\bwebdoc{Director:Schedule:End}

The \bbracket{Date-time-specification} is similar to what exists for Job scheduling.
See \vref{Date-time-specification} for more information.

\smallskip

An example schedule resource that is named \bvalue{WeeklyCycle} and connects a
director each Sunday at 2:05am and an on Monday through Saturday at 2:05am is:

\begin{bVerbatim}
Schedule {
  Name = "WeeklyCycle"
  Connect = MaxConnectTime=2h sun at 2:05
  Connect = MaxConnectTime=2h mon-sat at 2:05
}
\end{bVerbatim}

In this example, the Job for this client should be schedule to start between
2:05 and 4:05 during the week. The \bacula{} administrator will be able to use
commands such as \bcommandname{status client} or \bcommandname{estimate} betwen
2:05 and 4:05.

\section{The Message Resource}
\label{blb:MessagesResource3}
\index[general]{Message Resource}
\index[general]{Resource!Message }

Please see the
\bilink{Messages Resource}{blb:MessagesChapter} Chapter of this
manual for the details of the Messages Resource.

There must be at least one Message resource in the Client configuration file.

\bwebdoc{FDStatisticsResource}
\section{The Statistics Resource}
\index[general]{Resource!Statistics}
\index[general]{Statistics Resource}

The \bresourcename{Statistics} Resource defines the statistic collector function that can send
information to a Graphite instance, to a \acs{CSV} file or to \bconsolename{} with the
\bcommandname{statistics} command (See \vref{blb:StatisticsCommand} for more information).


\begin{bdescription}

\item [Statistics]
   \index[dir]{Statistics}
   \index[dir]{Directive!Statistics}
   Start of the \bresourcename{Statistics} resource.  Statistics directives are optional.

   \bwebdoc{FileDaemon:Statistics:Name}
\item [Name = \bbracket{name}]
   \index[dir]{Name}
   \index[dir]{Directive!Name}

   The Statistics directive \bdirectivename{name} is used by the system administrator. This
   directive is required.

   \bwebdoc{FileDaemon:Statistics:Description}
\item [Description = \bbracket{string}]
   \index[dir]{bdescription}
   \index[dir]{Directive!Description}

   The text field contains a description of the \bresourcename{Statistics} resource that will
   be displayed in the graphical user interface. This directive is optional.

   \bwebdoc{FileDaemon:Statistics:Interval}
\item [Interval = \bbracket{time-interval}]
   \index[dir]{Interval}
   \index[dir]{Directive!Interval}
   \index[dir]{Directive!Interval}

   The \bdirectivename{Intervall} directive instructs the Statistics collector thread how long
   it should sleep between every collection iteration. This directive is
   optional and the default value is \bdefaultvalue{300} seconds.

   \bwebdoc{FileDaemon:Statistics:Type}
\item [Type = \bbracket{CSV|Graphite}]
   \index[dir]{Type}
   \index[dir]{Directive!Type}

   The \bdirectivename{Type} directive specifies the Statistics backend, which may be one of the
   following: \bvalue{\acs{CSV}} or \bvalue{Graphite}. This directive is required.
\smallskip

   \acs{CSV} is a simple file level backend which saves all required metrics with the
   following format to the file: ``\bbracket{time}, \bbracket{metric}, \bbracket{value}\textbackslash{}n''

   Where \bbracket{time} is a standard Unix time (a number of seconds from 1/01/1970)
   with local timezone as returned by a system call \btool{time()}, \bbracket{metric} is a
   \bacula{} metric string and \bbracket{value} is a metric value which could be in numeric
   format (\texttt{int}/\texttt{float}) or a string \bvalue{"True"} or \bvalue{"False"} for boolean variable. The
   \acs{CSV} backend requires the \bvalueddirective{File}{~} parameter.
\smallskip

   Graphite is a network backend which will send all required metrics to a Graphite
   server. The Graphite backend requires the \bvalueddirective{Host}{~} and \bvalueddirective{Port}{~} directives to be set.

   If the Graphite server is not available, the metrics are automatically
   spooled in the working directory. When the server can be reached again,
   spooled metrics are despooled automatically and the spooling function is
   suspended.

   
   \bwebdoc{FileDaemon:Statistics:Metrics}
\item [Metrics = \bbracket{metricspec}]
   \index[dir]{Metrics}
   \index[dir]{Directive!Metrics}

   The \bdirectivename{Metrics} directive allow metric filtering and \bbracket{metricspec} is a filter which
   enables to use \bvalue{*} and \bvalue{?} characters to match the required metric name in the
   same way as found in shell wildcard resolution. You can exclude filtered
   metric with \bvalue{!}  prefix. You can define any number of filters for a single
   Statistics.  Metrics filter is executed in order as found in
   configuration. This directive is optional and if not used all available
   metrics will be saved by this collector backend.
   \smallskip

   Example:
\begin{bVerbatim}
# Include all metric starting with "bacula.jobs"
Metrics = "bacula.jobs.*"

# Exclude any metric starting with "bacula.jobs"
Metrics = "!bacula.jobs.*"
\end{bVerbatim}

   \bwebdoc{FileDaemon:Statistics:Prefix}
 \item [Prefix = \bbracket{string}]
   \index[dir]{Prefix}
   \index[dir]{Directive!Prefix}

   The \bdirectivename{Prefix} allows to alter the metrics name saved by collector to
   distinguish between different installations or daemons. The prefix string
   will be added to metric name as: ``\bbracket{prefix}.\bbracket{metric\_name}'' This directive is
   optional.

   \bwebdoc{FileDaemon:Statistics:Prefix}
 \item [File = \bbracket{filename}]
   \index[dir]{File}
   \index[dir]{Directive!File}
   
   The \bdirectivename{File} is used by the \acs{CSV} collector backend and point to the full
   path and filename of the file where metrics will be saved. With the \acs{CSV}
   type, the \bdirectivename{File} directive is required. The collector thread must have the
   permissions to write to the selected file or create a new file if the file
   doesn't exist.  If collector is unable to write to the file or create a new
   one then the collection terminates and an error message will be
   generated. The file is only open during the dump and is closed
   otherwise. Statistics file rotation could be executed by a mv shell command.

   \bwebdoc{FileDaemon:Statistics:Host}
 \item [Host = \bbracket{hostname}]
   \index[dir]{Host}
   \index[dir]{Directive!Host}

   The \bdirectivename{Host} directive is used for Graphite backend and specify the
   hostname or the \acs{IP} address of the Graphite server. When the directive Type
   is set to Graphite, the \bdirectivename{Host} directive is required.

   \bwebdoc{FileDaemon:Statistics:Port}
 \item [Host = \bbracket{number}]
   \index[dir]{Port}
   \index[dir]{Directive!Port}

   The \bdirectivename{Port} directive is used for Graphite backend and specify the \acs{TCP} port
   number of the Graphite server.  When the directive Type
   is set to Graphite, the \bdirectivename{Port} directive is required.

   
\bwebdoc{FileDaemon:Statistics:End}
\end{bdescription}


\section{Example Client Configuration File}
\label{blb:SampleClientConfiguration}
\index[general]{Example Client Configuration File }
\index[general]{File!Example Client Configuration }

An example File Daemon configuration file might be the following:

\begin{bVerbatim}
#
# Default  Bacula File Daemon Configuration file
#
#  For Bacula release 1.35.2 (16 August 2004) -- gentoo 1.4.16
#
# There is not much to change here except perhaps to
#   set the Director's name and File daemon's name
#   to something more appropriate for your site.
#
#
# List Directors who are permitted to contact this File daemon
#
Director {
  Name = rufus-dir
  Password = "/LqPRkX++saVyQE7w7mmiFg/qxYc1kufww6FEyY/47jU"
}
#
# Restricted Director, used by tray-monitor to get the
#   status of the file daemon
#
Director {
  Name = rufus-mon
  Password = "FYpq4yyI1y562EMS35bA0J0QC0M2L3t5cZObxT3XQxgxppTn"
  Monitor = yes
}
#
# "Global" File daemon configuration specifications
#
FileDaemon {                          # this is me
  Name = rufus-fd
  WorkingDirectory = $HOME/bacula/bin/working
  Pid Directory = $HOME/bacula/bin/working
}
# Send all messages except skipped files back to Director
Messages {
  Name = Standard
  director = rufus-dir = all, !skipped
}
\end{bVerbatim}
