\chapter{Console Configuration}
\label{blb:ConsoleConfChapter}
\index[general]{Configuration!Console}
\index[general]{Console Configuration}

\section{General}
\index[general]{General}

The Console configuration file is the simplest of all the configuration files,
and in general, you should not need to change it except for the password. It
simply contains the information necessary to contact the Director or
Directors.

For a general discussion of the syntax of configuration files and their
resources including the data types recognized by \bacula{}, please see
the \bilink{Configuration}{blb:ConfigureChapter} chapter of this manual.

The following Console Resource definition must be defined:

\section{The Director Resource}
\label{blb:DirectorResource3}
\index[general]{Director Resource}
\index[general]{Resource!Director}

The Director resource defines the attributes of the Director running on the
network. You may have multiple Director resource specifications in a single
Console configuration file. If you have more than one, you will be prompted to
choose one when you start the \textbf{Console} program.

\begin{bdescription}
\item [Director]
   \index[console]{Director}
   Start of the Director directives.

\bwebdoc{Console:Director:Name}
\item [Name = \bbracket{name}]
   \index[console]{Name}
   The director name used to select  among different Directors, otherwise, this
   name is not used.

\bwebdoc{Console:Director:DirPort}
\item [DIRPort = \bbracket{port-number}]
   \index[dir]{DIRPort}
   Specify the port to use to connect  to the Director. This value will most
   likely already be set to the value  you specified on the {\bf\verb:--:with-baseport}
   option of the  \btool{./configure} command. This port must be
   identical to the  \textbf{DIRport} specified in the \textbf{Director} resource of
   the \bilink{Director's configuration}{blb:DirectorChapter} file.  The
   default is 9101 so this directive is not normally specified.

\bwebdoc{Console:Director:Address}
\item [Address = \bbracket{address}]
   \index[dir]{Address}
   Where the address is a host name,  a fully qualified domain name, or a network
   address used to connect  to the Director.

\bwebdoc{Console:Director:Password}
\item [Password = \bbracket{password}]
   \index[dir]{Password}
   Where the password is the  password needed for the Director to accept the
   Console connection.  This password must be identical to the \textbf{Password}
   specified in  the \textbf{Director} resource of the
   \bilink{Director's configuration}{blb:DirectorChapter} file. This
   directive is required.

\bwebdoc{Console:Director:HistoryFile}
\item [HistoryFile = \bbracket{filename}]
   \index[console]{HistoryFile}
   Where the filename will be used to store the console command history. By default, the history file
   is set to \texttt{\$HOME/.bconsole\_history}

\bwebdoc{Console:Director:HistoryFileSize}
\item [HistoryFileSize = \bbracket{number-of-lines}]
   \index[console]{HistoryFileSize}
   Specify the history file size in lines. The default value is 100.

\input{ConsoleDirectorTls}
\bwebdoc{Console:Director:End2}
 \end{bdescription}

An actual example might be:

\begin{bVerbatim}
Director {
  Name = HeadMan
  address = rufus.cats.com
  password = xyz1erploit
}
\end{bVerbatim}

\section{The ConsoleFont Resource}
\index[general]{Resource!ConsoleFont}
\index[general]{ConsoleFont Resource}

The ConsoleFont resource is available only in the GNOME version of the
console. It permits you to define the font that you want used to display in
the main listing window.

\begin{bdescription}

\item [ConsoleFont]
   \index[console]{ConsoleFont}
   Start of the ConsoleFont directives.

\item [Name = \bbracket{name}]
   \index[console]{Name}
   The name of the font.

\item [Font = \bbracket{Pango Font Name}]
   \index[console]{Font}
   The string value given here defines the desired font. It  is specified in the
   Pango format. For example, the default specification is:

\begin{bVerbatim}
Font = "LucidaTypewriter 9"
\end{bVerbatim}

\end{bdescription}

Thanks to Phil Stracchino for providing the code for this feature.

An different example might be:

\begin{bVerbatim}
ConsoleFont {
  Name = Default
  Font = "Monospace 10"
}
\end{bVerbatim}

\section{The Console Resource}
\label{blb:ConsoleResource}
\index[general]{Console Resource}
\index[general]{Resource!Console}

There are three different kinds of consoles, which the administrator or
user can use to interact with the Director.  These three kinds of consoles
comprise three different security levels.

\begin{bitemize}
\item The first console type is an \textbf{anonymous} or \textbf{default}
   console, which has full privileges.  There is no console resource
   necessary for this type since the password is specified in the Director
   resource.  Typically you would use this \textbf{anonymous} console
   only for administrators.

\item The second type of console is a
   "named" or "restricted" console defined within a Console resource in
   both the Director's configuration file and in the Console's
   configuration file.  Both the names and the passwords in these two
   entries must match much as is the case for Client programs.

   This second type of console begins with absolutely no privileges except
   those explicitly specified in the Director's Console resource.  Note,
   the definition of what these restricted consoles can do is determined
   by the Director's conf file.

   Thus you may define within the Director's conf file multiple Consoles
   with different names and passwords, sort of like multiple users, each
   with different privileges.  As a default, these consoles can do
   absolutely nothing -- no commands what so ever.  You give them
   privileges or rather access to commands and resources by specifying
   access control lists in the Director's Console resource.  This gives the
   administrator fine grained control over what particular consoles (or
   users) can do.

\item The third type of console is similar to the above mentioned
   restricted console in that it requires a Console resource definition in
   both the Director and the Console.  In addition, if the console name,
   provided on the \textbf{Name =} directive, is the same as a Client name,
   the user of that console is permitted to use the \bcommandname{SetIP} command to
   change the Address directive in the Director's client resource to the IP
   address of the Console.  This permits portables or other machines using
   DHCP (non-fixed IP addresses) to "notify" the Director of their current
   IP address.

\end{bitemize}

The Console resource is optional and need not be specified. However, if it is
specified, you can use ACLs (Access Control Lists) in the Director's
configuration file to restrict the particular console (or user) to see only
information pertaining to his jobs or client machine.

You may specify as many Console resources in the console's conf file. If
you do so, generally the first Console resource will be used.  However, if
you have multiple Director resources (i.e. you want to connect to different
directors), you can bind one of your Console resources to a particular
Director resource, and thus when you choose a particular Director, the
appropriate Console configuration resource will be used. See the "Director"
directive in the Console resource described below for more information.

Note, the Console resource is optional, but can be useful for
restricted consoles as noted above.

\begin{bdescription}
\item [Console]
   \index[console]{Console}
   Start of the Console resource.

\bwebdoc{Console:Console:Name}
\item [Name = \bbracket{name}]
   \index[console]{Name}
   The Console name used to allow a restricted console to change
   its IP address using the SetIP command. The SetIP command must
   also be defined in the Director's conf CommandACL list.

\bwebdoc{Console:Console:Password}
\item [Password = \bbracket{password}]
   \index[console]{Password}
   If this password is supplied, then the password specified in the
   Director resource of you Console conf will be ignored.  See below
   for more details.

\bwebdoc{Console:Console:Director}
\item [Director = \bbracket{director-resource-name}]
   If this directive is specified, this Console resource will be
   used by bconsole when that particular director is selected
   when first starting bconsole.  I.e. it binds a particular console
   resource with its name and password to a particular director.

\bwebdoc{Console:Console:HeartbeatInterval}
\item [Heartbeat Interval = \bbracket{time-interval}]
   \index[console]{Heartbeat Interval}
   \index[console]{Directive!Heartbeat}
   This directive is optional and if specified will cause the Console to
   set a keepalive interval (heartbeat) in seconds on each of the sockets
   to communicate with the Director.  It is implemented only on systems
   (Linux, ...) that provide the \textbf{setsockopt} \texttt{TCP\_KEEPIDLE} function.
   The default value is \bdefaultvalue{zero}, which means no change is made to the socket.

\bwebdoc{Console:Console:CommCompression}
\item[CommCompression = \byesorno{}]
\index[console]{CommCompression}
\index[console]{Directive!CommCompression}
\index[console]{Compression}

If the two \bacula{} components (DIR, FD, SD, bconsole) have the comm line
compression enabled, the line compression will be enabled.  The default value
is yes.

In many cases, the volume of data transmitted across the communications line
can be reduced by a factor of three when this directive is
\bdefaultvalue{enabled}. In the case that the compression is not effective,
\bacula{} turns it off on a record by record basis.  \smallskip

If you are backing up data that is already compressed the comm line compression
will not be effective, and you are likely to end up with an average compression
ratio that is very small.  In this case, \bacula{} reports \bvalue{None} in the
Job report.
   
\input{ConsoleConsoleTls}
\bwebdoc{Console:Console:End1}
\end{bdescription}


The following configuration files were supplied by Phil Stracchino. For
example, if we define the following in the user's \bfilename{bconsole.conf} file (or
perhaps the \bfilename{bwx-console.conf} file):

\begin{bVerbatim}
Director {
   Name = MyDirector
   DIRport = 9101
   Address = myserver
   Password = "XXXXXXXXXXX"    # no, really.  this is not obfuscation.
}


Console {
   Name = restricted-user
   Password = "UntrustedUser"
}
\end{bVerbatim}

Where the Password in the Director section is deliberately incorrect, and the
Console resource is given a name, in this case \textbf{restricted-user}. Then
in the Director's \bfilename{bacula-dir.conf} file (not directly accessible by the user),
we define:

\begin{bVerbatim}
Console {
  Name = restricted-user
  Password = "UntrustedUser"
  JobACL = "Restricted Client Save"
  ClientACL = restricted-client
  StorageACL = main-storage
  ScheduleACL = *all*
  PoolACL = *all*
  FileSetACL = "Restricted Client's FileSet"
  CatalogACL = DefaultCatalog
  CommandACL = run
}
\end{bVerbatim}

the user logging into the Director from his Console will get logged in as
\textbf{restricted-user}, and he will only be able to see or access a Job with the
name \textbf{Restricted Client Save} a Client with the name
\textbf{restricted-client}, a Storage device \textbf{main-storage}, any Schedule or Pool,
a FileSet named \textbf{Restricted Client's FileSet}, a Catalog named
\textbf{DefaultCatalog}, and the only command he can use in the Console is the
\bcommandname{run} command. In other words, this user is rather limited in what he can see
and do with Bacula.

The following is an example of a bconsole conf file that can access
several Directors and has different Consoles depending on the director:

\begin{bVerbatim}
Director {
   Name = MyDirector
   DIRport = 9101
   Address = myserver
   Password = "XXXXXXXXXXX"    # no, really.  this is not obfuscation.
}

Director {
   Name = SecondDirector
   DIRport = 9101
   Address = secondserver
   Password = "XXXXXXXXXXX"    # no, really.  this is not obfuscation.
}

Console {
   Name = restricted-user
   Password = "UntrustedUser"
   Director = MyDirector
}

Console {
   Name = restricted-user-2
   Password = "A different UntrustedUser"
   Director = SecondDirector
}
\end{bVerbatim}

The second Director referenced at "secondserver" might look
like the following:

\begin{bVerbatim}
Console {
  Name = restricted-user-2
  Password = "A different UntrustedUser"
  JobACL = "Restricted Client Save"
  ClientACL = restricted-client
  StorageACL = second-storage
  ScheduleACL = *all*
  PoolACL = *all*
  FileSetACL = "Restricted Client's FileSet"
  CatalogACL = RestrictedCatalog
  CommandACL = run, restore
  WhereACL = "/"
}
\end{bVerbatim}

To use the same Console name on both Directors, you must create two
\texttt{bconsole.conf} to store the two Director/Console groups.


\section{Console Commands}
\index[general]{Console Commands}
\index[general]{Commands!Console}

For more details on running the console and its commands, please see the
\bxlink{Bacula Console}{blb:ConsoleChapter}{console}{chapter} of the \bconsoleman{}.

\section{Sample Console Configuration File}
\label{blb:SampleConfiguration2}
\index[general]{File!Sample Console Configuration}
\index[general]{Sample Console Configuration File}

An example Console configuration file might be the following:

\begin{bVerbatim}
#
# Bacula Console Configuration File
#
Director {
  Name = HeadMan
  address = "my_machine.my_domain.com"
  Password = Console_password
}
\end{bVerbatim}
