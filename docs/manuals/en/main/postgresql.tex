\chapter{Installing and Configuring \postgresql{}}
\label{blb:PostgreSqlChapter}
\index[general]{\postgresql{}!Installing and Configuring }
\index[general]{Installing and Configuring \postgresql{} }
\index[general]{Upgrading}

If you are considering using \postgresql{}, you should be aware
of their philosophy of upgrades, which could be
destabilizing for a production shop.  Basically at every major version
upgrade, you are required to dump your database in an ASCII format,
do the upgrade, and then reload your database (or databases). This is
because they frequently update the ``data format'' from version to
version, and they supply no tools to automatically do the conversion.
If you forget to do the ASCII dump, your database may become totally
useless because none of the new tools can access it due to the format
change, and the \postgresql{} server will not be able to start.

If you are building \postgresql{} from source, please be sure to add
the \textbf{{-}{-}enable-thread-safety} option when doing the \btool{./configure}
for \postgresql{}.

\section{Installing \postgresql{}}
\index[general]{\postgresql{}!Installing }

If you use the \btool{./configure {-}{-}with-postgresql=PostgreSQL-Directory}
statement for configuring \textbf{\bacula{}}, you will need \postgresql{} version 7.4
or later installed. NOTE! \postgresql{} versions earlier than 7.4 do not work
with \bacula{}. If \postgresql{} is installed in the standard system location, you
need only enter \textbf{{-}{-}with-postgresql} since the configure program will
search all the standard locations. If you install \postgresql{} in your home
directory or some other non-standard directory, you will need to provide the
full path with the \textbf{{-}{-}with-postgresql} option.

Installing and configuring \postgresql{} is not difficult but can be confusing
the first time. If you prefer, you may want to use a package provided by your
chosen operating system. Binary packages are available on most \postgresql{}
mirrors.

If you prefer to install from source, we recommend following the instructions
found in the \bfootref{http://www.postgresql.org/docs/}{\postgresql{} documentation}.

If you are using FreeBSD,
\bfootref{http://www.freebsddiary.org/postgresql.php}{this FreeBSD Diary article}
will be useful. Even if you are not using FreeBSD, the article will contain
useful configuration and setup information.

If you configure the Batch Insert code in \bacula{} (attribute inserts are
10 times faster), you \textbf{must} be using a \postgresql{} that was built with
the \textbf{{-}{-}enable-thread-safety} option, otherwise you will get
data corruption. Most major Linux distros have thread safety turned on, but
it is better to check.  One way is to see if the \postgresql{} library that
\bacula{} will be linked against references pthreads.  This can be done
with a command such as:

\begin{bVerbatim}
nm /usr/lib/libpq.a | grep pthread_mutex_lock
\end{bVerbatim}

The above command should print a line that looks like:

\begin{bVerbatim}
U pthread_mutex_lock
\end{bVerbatim}

if does, then everything is OK. If it prints nothing, do not enable batch
inserts when building \bacula{}.

After installing \postgresql{}, you should return to completing the installation
of \textbf{\bacula{}}. Later, after \bacula{} is installed, come back to this chapter
to complete the installation. Please note, the installation files used in the
second phase of the \postgresql{} installation are created during the \bacula{}
Installation. You must still come back to complete the second phase of the
\postgresql{} installation even if you installed binaries (e.g. rpm, deb,
\ldots{}).


\label{blb:PostgreSQL_configure}
\section{Configuring \postgresql{}}
\index[general]{\postgresql{}!Configuring \postgresql{} -- }

At this point, you should have built and installed \postgresql{}, or already have
a running \postgresql{}, and you should have configured, built and installed \textbf{\bacula{}}.
If not, please complete these items before proceeding.

Please note that the \btool{./configure} used to build \textbf{\bacula{}} will need to
include \textbf{{-}{-}with-postgresql=PostgreSQL-directory}, where
\textbf{PostgreSQL-directory} is the directory name that you specified on the
\btool{./configure} command for configuring \postgresql{} (if you didn't specify a
directory or \postgresql{} is installed in a default location, you do not need to
specify the directory). This is needed so that \bacula{} can find the necessary
include headers and library files for interfacing to \postgresql{}.

An important thing to note here is that \textbf{\bacula{}} makes two connections
to the \postgresql{} server for each backup job that is currently running.  If
you are intending to run a large number of concurrent jobs, check the value
of \textbf{max\_connections} in your \postgresql{} configuration file to ensure
that it is larger than the setting \textbf{Maximum Concurrent Jobs}
in your director configuration.
\textbf{Setting this too low will result in some backup jobs failing to run correctly!}

\textbf{\bacula{}} will install scripts for manipulating the database (create,
delete, make tables etc) into the main installation directory. These files
will be of the form \bfilename{*\_bacula\_*} (e.g. \bfilename{create\_bacula\_database}). These files
are also available in the \bbracket{bacula-src}/src/cats directory after
running \btool{./configure}. If you inspect \bfilename{create\_bacula\_database}, you will see
that it calls \bfilename{create\_postgresql\_database}. The \bfilename{*\_bacula\_*} files are
provided for convenience. It doesn't matter what database you have chosen;
\bfilename{create\_bacula\_database} will always create your database.

Now you will create the \bacula{} \postgresql{} database and the tables that \bacula{}
uses. These instructions assume that you already have \postgresql{} running. You
will need to perform these steps as a user that is able to create new
databases. This can be the \postgresql{} user (on most systems, this is the \texttt{pgsql}
user).

\begin{benumerate}
\item cd \bbracket{install-directory}

   This directory contains the \bacula{} catalog interface routines.

\item Create the database owner (\textbf{bacula})
   On many systems, the \postgresql{} master
   owner is \texttt{pgsql} and on others such as Red Hat and Fedora it
   is \texttt{postgres}.  You can find out which it is by examining your \bfilename{/etc/passwd}
   file.  To create a new user under either your name or with say the name
   \texttt{bacula}, you can do the following:

\begin{bVerbatim}
su
(enter root password)
su pgsql (for rpm systems or postgres for Debian systems)
createuser bacula
Shall the new user be allowed to create databases? (y/n) y
Shall the new user be allowed to create more new users? (y/n) (choose
      what you want)
exit
\end{bVerbatim}

If you have a newer \postgresql{}, you may need to use the \textbf{-s} option on the \btool{createuser} command.  Example:

\begin{bVerbatim}
  createuser -s bacula
\end{bVerbatim}

   Normally the \texttt{bacula} user must be able to create new databases,
   if you use the script in the next item,
   or you will have to create one for it, but it does not need to
   create new users.

\item \btool{./create\_bacula\_database}

   This script creates the \postgresql{} \textbf{bacula} database.
   Before running this command, you should carefully think about
   what encoding sequence you want for the text fields (paths, files, \ldots{}).
   We strongly recommend that you use the default value of SQL\_ASCII
   that is in the \btool{create\_bacula\_database} script.  Please be warned
   that if you change this value, your backups may fail.  After running
   the script, you can check with the command:

\begin{bVerbatim}
psql -l
\end{bVerbatim}

and the column marked \textbf{Encoding} should be \textbf{SQL\_ASCII} for
all your \bacula{} databases (normally \textbf{bacula}).

\item \btool{./make\_bacula\_tables}
  
  This script creates the \postgresql{} tables used by \textbf{\bacula{}}.
\item \btool{./grant\_bacula\_privileges}

  This script creates the database user \textbf{bacula}  with restricted access
  rights. You may  want to modify it to suit your situation. Please note that
  this database is not password protected.
\end{benumerate}

Each of the three scripts (\btool{create\_bacula\_database}, \btool{make\_bacula\_tables}, and
\btool{grant\_bacula\_privileges}) allows the addition of a command line argument.
This can be useful for specifying the user name. For example, you might need
to add \textbf{-h hostname} to the command line to specify a remote database
server.

To take a closer look at the access privileges that you have setup with the
above, you can do:

\begin{bVerbatim}
PostgreSQL-directory/bin/psql --command \dp bacula
\end{bVerbatim}

For people who are not Postgresql experts, it is sometimes difficult
to get the authorization working correctly with Bacula.
One simple, but not recommended way, to authorize Bacula is
to modify your {\bf pg\_hba.conf} file (in /var/lib/pgsql/data 
or in /var/lib/postgresql/8.x or in /etc/postgres/8.x/main on
other distributions) from:

\begin{bVerbatim}
local   all    all        ident  sameuser
\end{bVerbatim}
to
\begin{bVerbatim}
local   all    all        trust
\end{bVerbatim}

This is a quick way to solve the problem, but it is not always a good thing
to do from a security standpoint.  However, it allows one to run
my regression scripts without having a password.

A more secure way to perform database authentication is with md5
password hashes.  Begin by editing the {\bf pg\_hba.conf} file, and
above the existing {\bf local} and {\bf host} lines, add the line:

\begin{bVerbatim}
local bacula bacula md5
\end{bVerbatim}

then restart the \postgresql{} database server (frequently, this can be done
using \btool{/etc/init.d/postgresql restart} or \btool{service postgresql restart}) to
put this new authentication rule into effect.

\smallskip
More detailed information on the {\bf pg\_hba.conf} file can be found at
\bulink{PostgreSQL pg\_hba.conf Documentation}
{https://www.postgresql.org/docs/10/auth-pg-hba-conf.html}

\smallskip

Next, become the Postgres administrator, postgres, either by logging
on as the postgres user, or by using su to become root and then using
{\bf su - postgres} or {\bf su - pgsql} to become postgres.  
Add a password to the {\bf bacula} database for the {\bf bacula} user using:

\begin{bVerbatim}
$ psql bacula
bacula=# alter user bacula with password 'secret';
ALTER USER
bacula=# \q
\end{bVerbatim}

You'll have to add this password to two locations in the
\bfilename{bacula-dir.conf} file: once to the Catalog resource and once to the
RunBeforeJob entry in the BackupCatalog Job resource.  With the
password in place, these two lines should look something like:

\begin{bVerbatim}
dbname = bacula; user = bacula; password = "secret"
\end{bVerbatim}
\ldots{} and \ldots{}
\begin{bVerbatim}
# WARNING!!! Passing the password via the command line is insecure.
# see comments in make_catalog_backup.pl for details.
RunBeforeJob = "/etc/make_catalog_backup.pl MyCatalog"
\end{bVerbatim}

Naturally, you should choose your own significantly more random
password, and ensure that the \bfilename{bacula-dir.conf} file containing this
password is readable only by the \texttt{root} user.

Even with the files containing the database password properly
restricted, there is still a security problem with this approach: on
some platforms, the environment variable that is used to supply the
password to \postgresql{} is available to all users of the
local system.  To eliminate this problem, the \postgresql{} team have
deprecated the use of the environment variable password-passing
mechanism and recommend the use of a \bfilename{.pgpass} file instead.  To use
this mechanism, create a file named \bfilename{.pgpass} containing the single
line:

\begin{bVerbatim}
localhost:5432:bacula:bacula:secret
\end{bVerbatim}

This file should be copied into the home directory of all accounts
that will need to gain access to the database: typically, \texttt{root},
\texttt{bacula}, and any users who will make use of any of the console
programs.  The files must then have the owner and group set to match
the user (so \texttt{root}\string:\texttt{root} for the copy in \bdirectoryname{~root}, and so on), and the mode
set to 600, limiting access to the owner of the file.

\section{Re-initializing the Catalog Database}
\index[general]{Database!Re-initializing the Catalog }
\index[general]{Re-initializing the Catalog Database }

After you have done some initial testing with \textbf{\bacula{}}, you will probably
want to re-initialize the catalog database and throw away all the test Jobs
that you ran. To do so, you can do the following:

\begin{bVerbatim}
cd <install-directory>
./drop_bacula_tables
./make_bacula_tables
./grant_bacula_privileges
\end{bVerbatim}

Please note that all information in the database will be lost and you will be
starting from scratch. If you have written on any Volumes, you must write an
end of file mark on the volume so that \bacula{} can reuse it. Do so with:

\begin{bVerbatim}
(stop Bacula or unmount the drive)
mt -f /dev/nst0 rewind
mt -f /dev/nst0 weof
\end{bVerbatim}

Where you should replace \bfilename{/dev/nst0} with the appropriate tape drive
device name for your machine.

\section{Installing \postgresql{} from \acsp{RPM}}
\index[general]{\postgresql{}!Installing from \acsp{RPM}}
\index[general]{Installing \postgresql{} from \acsp{RPM}}
If you are installing \postgresql{} from \acsp{RPM}, you will need to install
both the \postgresql{} binaries and the client libraries.  The client
libraries are usually found in a \textbf{devel} or \textbf{dev} package, so you must
install the following for rpms:

\begin{bVerbatim}
postgresql
postgresql-devel
postgresql-server
postgresql-libs
\end{bVerbatim}


and the following for debs:

\begin{bVerbatim}
postgresql
postgresql-common
postgresql-client
postgresql-client-common
libpq5
libpq-dev
\end{bVerbatim}


These will be similar with most other package managers too.  After
installing from rpms, you will still need to run the scripts that set up
the database and create the tables as described above.

\section{Converting from MySQL to PostgreSQL}
\index[general]{PostgreSQL!Converting from MySQL to }
\index[general]{Converting from MySQL to PostgreSQL }

There are various scripts available that allow you to convert from MySQL 
to PostgreSQL database format.  The first step is to backup your existing
MySQL database, then shutdown Bacula.

\smallskip
One such conversion program from MySQL to PostgreSQL can be found at:
\bulink{MySQL to PostgreSQL Conversion Scripts}
{https://github.com/wanderleihuttel/bacula-utils/tree/master/convert_mysql_to_postgresql}

\section{Upgrading \postgresql{}}
\index[general]{Upgrading \postgresql{}}
\index[general]{Upgrading!\postgresql{}}
\index[general]{Upgrading}
\baculaenterprise{} is built with the \postgresql{} that is appropriate for each Operating System platform.  Each time that \postgresql{} is updated, the \bacula{} binaries must be updated.
If you upgrade PostgreSQL, on older PostgreSQLs, you needed to 
rebuild Bacula for the specific version of PostgreSQL.  However,
on more recent versions, this is no longer necessary. 
If you experience problems after upgrading PostgreSQL, you might
try rebuilding Bacula.

\section{Tuning \postgresql{}}
\index[general]{Tuning}

If you despool attributes for many jobs at the same time, you can tune the
sequence object for the \texttt{FileId} field.
\begin{bVerbatim}
psql -Ubacula bacula

ALTER SEQUENCE file_fileid_seq CACHE 1000;
\end{bVerbatim}

\section{Credits}
\index[general]{Credits }
Many thanks to Dan Langille for writing the \postgresql{} driver. This will
surely become the most popular database that \bacula{} supports.
