\chapter{Autochanger Support}
\label{blb:AutochangersChapter}
\index[general]{Support!Autochanger}
\index[general]{Autochanger Support}

\bacula{} provides autochanger support for reading and writing tapes.  In
order to work with an autochanger, \bacula{} requires a number of things, each of
which is explained in more detail after this list:

\begin{bitemize}
\item A script that actually controls the autochanger according  to commands
   sent by \bacula{}. We furnish such a script  that works with \mtx{} found in
   the \textbf{depkgs} distribution.

\item That each Volume (tape) to be used must be defined in the Catalog and
   have a Slot number assigned to it so that \bacula{} knows where the Volume is
   in the autochanger. This is generally done with the \bcommandname{label} command, but
   can also done after the tape is labeled using the \bcommandname{update slots}
   command.  See below for more details. You must pre-label the tapes manually
   before using them.

\item Modifications to your Storage daemon's Device configuration  resource to
   identify that the device is a changer, as well  as a few other parameters.

\item You should also modify your Storage resource definition  in the
   Director's configuration file so that you are automatically prompted for the
   Slot when labeling a Volume.

\item You need to ensure that your Storage daemon (if not running as root)
   has access permissions to both the tape drive and the control device.

\item You need to have \textbf{Autochanger = yes} in your Storage resource
   in your \bfilename{bacula-dir.conf} file so that you will be prompted for the
   slot number when you label Volumes.
\end{bitemize}

In version 1.37 and later, there is a new \bilink{Autochanger resource}{blb:AutochangerRes}
that permits you to group Device resources thus
creating a multi-drive autochanger. If you have an autochanger,
you \textbf{must} use this new resource.

\bacula{} uses its own \mtxchanger{} script to interface with a program
that actually does the tape changing.  Thus in principle, \mtxchanger{}
can be adapted to function with any autochanger program, or you can
call any other script or program. The current
version of \mtxchanger{} works with the \mtx{} program.  However,
FreeBSD users have provided a script in the \textbf{\bdirectoryname{examples/autochangers}}
directory that allows \bacula{} to use the \btool{chio} program.

\bacula{} also supports autochangers with barcode
readers. This support includes two Console commands: \bcommandname{label barcodes}
and \bcommandname{update slots}. For more details on these commands, see the ``Barcode
Support'' section below.

Current \bacula{} autochanger support does not include cleaning, stackers, or
silos. Stackers and silos are not supported because \bacula{} expects to
be able to access the slots randomly.
However, if you are very careful to setup \bacula{} to access the Volumes
in the autochanger sequentially, you may be able to make \bacula{}
work with stackers (gravity feed and such).

Support for multi-drive
autochangers requires the \bilink{Autochanger resource}{blb:AutochangerRes}
introduced in version 1.37.  This resource is also recommended for single
drive autochangers.

In principle, if \mtx{} will operate your changer correctly, then it is
just a question of adapting the \mtxchanger{} script (or selecting one
already adapted) for proper interfacing.
%You can find a list of autochangers
%supported by \mtx{} at the following link:
%\bref{http://mtx.opensource-sw.net/compatibility.php}{mtx.opensource-sw.net/compatibility.php}.
%The home page for the \mtx{} project can be found at:
%\bref{http://mtx.opensource-sw.net/}{mtx.opensource-sw.net/}.

Note, we have feedback from some users that there are certain
incompatibilities between the Linux kernel and \mtx{}.  For example between
kernel 2.6.18-8.1.8.el5 of CentOS and RedHat and version 1.3.10 and 1.3.11
of \mtx{}.  This was fixed by upgrading to a version 2.6.22 kernel.

In addition, apparently certain versions of \mtx{}, for example, version
1.3.11 limit the number of slots to a maximum of 64. The solution was to
use version 1.3.10.

If you are having troubles, please use the \bcommandname{auto} command in the \btool{btape}
program to test the functioning of your autochanger with \bacula{}. When
\bacula{} is running, please remember that for many distributions (e.g. FreeBSD,
Debian, \ldots{}) the Storage daemon runs as \textbf{bacula.tape} rather than \textbf{root.root},
so you will need to ensure that the Storage daemon has sufficient
permissions to access the autochanger.

Some users have reported that the the Storage daemon blocks under certain
circumstances in trying to mount a volume on a drive that has a different
volume loaded.  As best we can determine, this is simply a matter of
waiting a bit.  The drive was previously in use writing a Volume, and
sometimes the drive will remain \texttt{BLOCKED} for a good deal of time (up to 7
minutes on a slow drive) waiting for the cassette to rewind and to unload
before the drive can be used with a different Volume.

\label{blb:SCSI devices}
\section{Knowing What \acs{SCSI} Devices You Have}
\index[general]{Have!Knowing What \acs{SCSI} Devices You}
\index[general]{Knowing What \acs{SCSI} Devices You Have}
\index[general]{SCSI devices}
\index[general]{devices!SCSI}

Under Linux, you can

\begin{bVerbatim}
cat /proc/scsi/scsi
\end{bVerbatim}

to see what \acs{SCSI} devices you have available. You can also:

\begin{bVerbatim}
cat /proc/scsi/sg/device_hdr /proc/scsi/sg/devices
\end{bVerbatim}

to find out how to specify their control address (\bfilename{/dev/sg0} for the
first, \bfilename{/dev/sg1} for the second, \ldots{}) on the \textbf{\bdirectivename{Changer Device =}}
\bacula{} directive.

You can also use the excellent  \btool{lsscsi} tool.
\begin{bVerbatim}
$ lsscsi -g
 [1:0:2:0]    tape    SEAGATE  ULTRIUM06242-XXX 1619  /dev/st0  /dev/sg9
 [1:0:14:0]   mediumx STK      L180             0315  /dev/sch0 /dev/sg10
 [2:0:3:0]    tape    HP       Ultrium 3-SCSI   G24S  /dev/st1  /dev/sg11
 [3:0:0:0]    enclosu HP       A6255A           HP04  -         /dev/sg3
 [3:0:1:0]    disk    HP 36.4G ST336753FC       HP00  /dev/sdd  /dev/sg4
\end{bVerbatim}

For more detailed information on what \acs{SCSI} devices you have please see
the \bxlink{Linux \acs{SCSI} Tricks}{blb:SCSITricks}{problems}{section} of the
 \bxlink{Tape Testing}{blb:TapeTestingChapter}{problems}{chapter} of the
 \bproblemman{}.

Under FreeBSD, you can use:

\begin{bVerbatim}
camcontrol devlist
\end{bVerbatim}

To list the \acs{SCSI} devices as well as the \bfilename{/dev/passn} that you will use on
the \bacula{} \bdirectivename{Changer Device =} directive.

Please check that your Storage daemon has permission to access this
device.

The following tip for FreeBSD users comes from Danny Butroyd:
on reboot \bacula{} will NOT have permission to
control the device \bfilename{/dev/pass0} (assuming this is your changer device).
To get around this just edit the \bfilename{/etc/devfs.conf} file and add the
following to the bottom:

\begin{bVerbatim}
own     pass0   root:bacula
perm    pass0   0666
own     nsa0.0  root:bacula
perm    nsa0.0  0666
\end{bVerbatim}

This gives the bacula group permission to write to the \bfilename{nsa0.0} device
too just to be on the safe side.   To bring these changes into effect
just run:

\begin{bVerbatim}
/etc/rc.d/devfs restart
\end{bVerbatim}

Basically this will stop you having to manually change permissions on these
devices to make \bacula{} work when operating the AutoChanger after a reboot.

\label{blb:scripts}
\section{Example Scripts}
\index[general]{Scripts!Example}
\index[general]{Example Scripts}

Please read the sections below so that you understand how autochangers work
with \bacula{}. Although we supply a default \mtxchanger{} script, your
autochanger may require some additional changes. If you want to see examples
of configuration files and scripts, please look in the \bdirectoryname{\bbracket{bacula-src}/examples/devices}
directory where you will find an example \bfilename{HP-autoloader.conf}
\bacula{} Device resource, and several \mtxchanger{} scripts that have been modified to work with different
autochangers.

\label{blb:Slots}

\section{Slots}
\index[general]{Slots}

To properly address autochangers, \bacula{} must know which Volume is in each
\textbf{slot} of the autochanger. Slots are where the changer cartridges reside
when not loaded into the drive. \bacula{} numbers these slots from one to the
number of cartridges contained in the autochanger.

\bacula{} will not automatically use a Volume in your autochanger unless it is
labeled and the slot number is stored in the catalog and the Volume is marked
as InChanger. This is because it must know where each volume is (slot) to
be able to load the volume.

For each Volume in your
changer, you will, using the Console program, assign a slot. This information
is kept in \textbf{\bacula{}'s} catalog database along with the other data for the
volume. If no slot is given, or the slot is set to zero, \bacula{} will not
attempt to use the autochanger even if all the necessary configuration records
are present. When doing a \bcommandname{mount} command on an autochanger, you must
specify which slot you want mounted.  If the drive is loaded with a tape
from another slot, it will unload it and load the correct tape, but
normally, no tape will be loaded because an \bcommandname{unmount} command causes
\bacula{} to unload the tape in the drive.


You can check if the Slot number and InChanger flag are set by doing a:
\begin{bVerbatim}
list Volumes
\end{bVerbatim}

in the Console program.

\label{blb:mult}
\section{Multiple Devices}
\index[general]{Devices!Multiple}
\index[general]{Multiple Devices}

Some autochangers have more than one read/write device (drive). The
new \bilink{Autochanger resource}{blb:AutochangerRes} introduced in version
1.37 permits you to group Device resources, where each device
represents a drive. The Director may still reference the Devices (drives)
directly, but doing so, bypasses the proper functioning of the
drives together.  Instead, the Director (in the Storage resource)
should reference the Autochanger resource name. Doing so permits
the Storage daemon to ensure that only one drive uses the \mtxchanger{}
script at a time, and also that two drives don't reference the
same Volume.

Multi-drive requires the use of the \textbf{\bdirectivename{Drive Index}} directive in
the \bresourcename{Device} resource of the Storage daemon's
configuration file. Drive numbers or the Device Index are numbered beginning
at zero (\bdefaultvalue{0}), which is the default. To use the second Drive in an autochanger, you
need to define a second \bresourcename{Device} resource and set the \bdirectivename{Drive Index} to 1 for
that device. In general, the second device will have the same \textbf{\bdirectivename{Changer Device}}
(control channel) as the first drive, but a different \textbf{\bdirectivename{Archive Device}}.

As a default, \bacula{} jobs will prefer to write to a Volume that is
already mounted. If you have a multiple drive autochanger and you want
\bacula{} to write to more than one Volume in the same Pool at the same
time, you will need to set \bilink{Prefer Mounted Volumes}{blb:PreferMountedVolumes}
in the Directors Job resource to \textbf{no}. This will cause
the Storage daemon to maximize the use of drives.


\label{blb:ConfigRecords}
\section{Device Configuration Records}
\index[general]{Records!Device Configuration}
\index[general]{Device Configuration Records}

Configuration of autochangers within \bacula{} is done in the Device resource of
the Storage daemon. Four records: \textbf{\bdirectivename{Autochanger}}, \textbf{\bdirectivename{Changer Device}},
\textbf{\bdirectivename{Changer Command}}, and \textbf{\bdirectivename{Maximum Changer Wait}} control how \bacula{} uses
the autochanger.

These four records, permitted in \textbf{\bresourcename{Device}} resources, are described in
detail below. Note, however, that the \textbf{\bdirectivename{Changer Device}} and the
\textbf{\bdirectivename{Changer Command}} directives are not needed in the \bresourcename{Device} resource
if they are present in the \textbf{\bresourcename{Autochanger}} resource.

\begin{bdescription}

\item [Autochanger = \byesorno{} ]
   \index[sd]{Autochanger}
   The \textbf{Autochanger} record specifies that the current device  is or is not
an autochanger. The default is \bdefaultvalue{no}.

\item [Changer Device = \bbracket{device-name}]
   \index[sd]{Changer Device}
   In addition to the Archive Device name, you must specify a  \textbf{\bdirectivename{Changer Device}} name.
   This is because most autochangers are  controlled through a
   different device than is used for reading and  writing the cartridges. For
example, on Linux, one normally uses the generic \acs{SCSI} interface for
controlling the autochanger, but the standard \acs{SCSI} interface for reading and
writing the  tapes. On Linux, for the \textbf{Archive Device = \bfilename{/dev/nst0}},  you
would typically have \textbf{Changer Device = \bfilename{/dev/sg0}}.  Note, some of the more
advanced autochangers will locate the changer device on \bfilename{/dev/sg1}. Such
devices typically have  several drives and a large number of tapes.

On FreeBSD systems, the changer device will typically be on \bfilename{/dev/pass0}
through \bfilename{/dev/passn}.

On Solaris, the changer device will typically be some file under \textbf{\bfilename{/dev/rdsk}}.

Please ensure that your Storage daemon has permission to access this device.

\item [Changer Command = \bbracket{command}]
   \index[sd]{Changer Command}
   This record is used to specify the external program to call  and what
arguments to pass to it. The command is assumed to be  a standard program or
shell script that can be executed by  the operating system. This command is
invoked each time that \bacula{} wishes to manipulate the autochanger.  The
following substitutions are made in the \textbf{command}  before it is sent to
the operating system for execution:


\begin{bVerbatim}
%% = %
%a = archive device name
%c = changer device name
%d = changer drive index base 0
%f = Client's name
%j = Job name
%o = command  (loaded, load, or unload)
%s = Slot base 0
%S = Slot base 1
%v = Volume name
\end{bVerbatim}

An actual example for using \mtx{} with the  \mtxchanger{} script (part
of the \bacula{} distribution) is:

\begin{bVerbatim}
Changer Command = "/opt/bacula/scripts/mtx-changer %c %o %S %a %d"
\end{bVerbatim}

Where you will need to adapt the \bdirectoryname{/opt/bacula} to be  the actual path on
your system where the \mtxchanger{} script  resides.  Details of the three
commands currently used by \bacula{}  (\bcommandname{loaded}, \bcommandname{load}, \bcommandname{unload}) as well as the
output expected by  \bacula{} are give in the \textbf{\bacula{} Autochanger Interface}
section below.

\item [Maximum Changer Wait = \bbracket{time}]
   \index[sd]{Maximum Changer Wait}
   This record is used to define the maximum amount of time that \bacula{}
   will wait for an autoloader to respond to a command (e.g.  \bcommandname{load}).  The
   default is set to \bdefaultvalue{120} seconds.  If you have a slow autoloader you may
   want to set it longer.

If the autoloader program fails to respond in this time, it  will be killed
and \bacula{} will request operator intervention.

\item [Drive Index = \bbracket{number}]
   \index[sd]{Drive Index}
   This record allows you to tell \bacula{} to use the second or subsequent
   drive in an autochanger with multiple drives.  Since the drives are
   numbered from zero, the second drive is defined by

\begin{bVerbatim}
Drive Index = 1
\end{bVerbatim}

To use the second drive, you need a second Device resource definition  in the
\bacula{} configuration file. See the \bilink{Multiple Drive}{blb:mult} section above  in this
chapter for more information.
\end{bdescription}

In addition, for proper functioning of the Autochanger, you must
define an \bresourcename{Autochanger} resource.
\input{autochangerres}

\label{blb:example}
\section{An Example Configuration File}
\index[general]{Example Configuration File}
\index[general]{File!Example Configuration}

The following two resources implement an autochanger:

\begin{bVerbatim}
Autochanger {
  Name = "Autochanger"
  Device = DDS-4
  Changer Device = /dev/sg0
  Changer Command = "/opt/bacula/scripts/mtx-changer %c %o %S %a %d"
}

Device {
  Name = DDS-4
  Media Type = DDS-4
  Archive Device = /dev/nst0    # Normal archive device
  Autochanger = yes
  LabelMedia = no
  AutomaticMount = yes
  AlwaysOpen = yes
}
\end{bVerbatim}

where you will adapt the \textbf{\bdirectivename{Archive Device}}, the \textbf{\bdirectivename{Changer Device}}, and
the path to the \textbf{\bdirectivename{Changer Command}} to correspond to the values used on your
system.

\section{A Multi-drive Example Configuration File}
\index[general]{Multi-drive Example Configuration File}

The following resources implement a multi-drive autochanger:

\begin{bVerbatim}
Autochanger {
  Name = "Autochanger"
  Device = Drive-1,Drive-2
  Changer Device = /dev/sg0
  Changer Command = "/opt/bacula/scripts/mtx-changer %c %o %S %a %d"
}

Device {
  Name = Drive-1
  Drive Index = 0
  Media Type = DDS-4
  Archive Device = /dev/nst0    # Normal archive device
  Autochanger = yes
  LabelMedia = no
  AutomaticMount = yes
  AlwaysOpen = yes
}

Device {
  Name = Drive-2
  Drive Index = 1
  Media Type = DDS-4
  Archive Device = /dev/nst1    # Normal archive device
  Autochanger = yes
  LabelMedia = no
  AutomaticMount = yes
  AlwaysOpen = yes
}
\end{bVerbatim}

where you will adapt the \textbf{\bdirectivename{Archive Device}}, the \textbf{\bdirectivename{Changer Device}}, and
the path to the \textbf{\bdirectivename{Changer Command}} to correspond to the values used on your
system.

\label{blb:SpecifyingSlots}
\section{Specifying Slots When Labeling}
\index[general]{Specifying Slots When Labeling}
\index[general]{Labeling!Specifying Slots When}

If you add an \textbf{\bdirectivename{Autochanger = yes}} record to the Storage resource in your
Director's configuration file, the \bacula{} Console will automatically prompt
you for the slot number when the Volume is in the changer when
you \bcommandname{add} or \bcommandname{label} tapes for that Storage device. If your
\mtxchanger{} script is properly installed, \bacula{} will automatically
load the correct tape during the \bcommandname{label} command.

You must also set
\textbf{\bdirectivename{Autochanger = yes}} in the Storage daemon's Device resource
as we have described above in
order for the autochanger to be used. Please see the
\bilink{Storage Resource}{blb:Autochanger1} in the Director's chapter
and the
\bilink{Device Resource}{blb:Autochanger} in the Storage daemon
chapter for more details on these records.

Thus all stages of dealing with tapes can be totally automated. It is also
possible to set or change the Slot using the \bcommandname{update} command in the
Console and selecting \textbf{Volume Parameters} to update.

Even though all the above configuration statements are specified and correct,
\bacula{} will attempt to access the autochanger only if a \textbf{slot} is non-zero
in the catalog Volume record (with the Volume name).

If your autochanger has barcode labels, you can label all the Volumes in
your autochanger one after another by using the \bcommandname{label barcodes} command.
For each tape in the changer containing a barcode, \bacula{} will mount the tape
and then label it with the same name as the barcode. An appropriate Media
record will also be created in the catalog. Any barcode that begins with the
same characters as specified on the ``\bdirectivename{CleaningPrefix=xxx} command, will be
treated as a cleaning tape, and will not be labeled.

For example with 
\begin{bVerbatim}
Pool {
  Name ...
  Cleaning Prefix = "CLN"
}
\end{bVerbatim}
any slot containing a barcode of \texttt{CLNxxxx} will be treated as a cleaning tape
and will not be mounted.

Please note that Volumes must be pre-labeled to be automatically used in
the autochanger during a backup.  If you do not have a barcode reader, this
is done manually (or via a script).

\section{Changing Cartridges}
\index[general]{Changing Cartridges}
If you wish to insert or remove cartridges in your autochanger or
you manually run the \mtx{} program, you must first tell \bacula{}
to release the autochanger by doing:

\begin{bVerbatim}
unmount
\end{bVerbatim}
Change cartridges and/or run \mtx{}
\begin{bVerbatim}
mount
\end{bVerbatim}

If you do not do the \bcommandname{unmount} before making such a change, \bacula{}
will become completely confused about what is in the autochanger
and may stop function because it expects to have exclusive use
of the autochanger while it has the drive mounted.


\label{blb:Magazines}
\section{Dealing with Multiple Magazines}
\index[general]{Dealing with Multiple Magazines}
\index[general]{Magazines!Dealing with Multiple}

If you have several magazines or if you insert or remove cartridges from a
magazine, you should notify \bacula{} of this. By doing so, \bacula{} will as
a preference, use Volumes that it knows to be in the autochanger before
accessing Volumes that are not in the autochanger. This prevents unneeded
operator intervention.

If your autochanger has barcodes (machine readable tape labels), the task of
informing \bacula{} is simple. Every time, you change a magazine, or add or
remove a cartridge from the magazine, simply do

\begin{bVerbatim}
unmount
\end{bVerbatim}
Remove magazine

Insert new magazine
\begin{bVerbatim}
update slots
mount
\end{bVerbatim}

in the Console program. This will cause \bacula{} to request the autochanger to
return the current Volume names in the magazine. This will be done without
actually accessing or reading the Volumes because the barcode reader does this
during inventory when the autochanger is first turned on. \bacula{} will ensure
that any Volumes that are currently marked as being in the magazine are marked
as no longer in the magazine, and the new list of Volumes will be marked as
being in the magazine. In addition, the Slot numbers of the Volumes will be
corrected in \bacula{}'s catalog if they are incorrect (added or moved).

If you do not have a barcode reader on your autochanger, you have several
alternatives.

\begin{benumerate}
\item You can manually set the Slot and InChanger flag using  the
  \bcommandname{update volume} command in the Console (quite  painful).

\item You can issue a

\begin{bVerbatim}
update slots scan
\end{bVerbatim}

   command that will cause \bacula{} to read the label on each  of the cartridges in
   the magazine in turn and update the  information (Slot, InChanger flag) in the
   catalog. This  is quite effective but does take time to load each cartridge
   into the drive in turn and read the Volume label.

\item You can modify the \mtxchanger{} script so that it simulates  an
   autochanger with barcodes. See below for more details.
\end{benumerate}

\label{blb:simulating}
\section{Simulating Barcodes in your Autochanger}
\index[general]{Autochanger!Simulating Barcodes in your}
\index[general]{Simulating Barcodes in your Autochanger}

You can simulate barcodes in your autochanger by making the \mtxchanger{}
script return the same information that an autochanger with barcodes would do.
This is done by commenting out the one and only line in the \textbf{list)} case,
which is:

\begin{bVerbatim}
${MTX} -f $ctl status | grep " *Storage Element [0-9]*:.*Full" | awk "{print \$3 \$4}"
                      | sed "s/Full *\(:VolumeTag=\)*//"
\end{bVerbatim}

at approximately line 99 by putting a \# in column one of that line, or by
simply deleting it. Then in its place add a new line that prints the contents
of a file. For example:

\begin{bVerbatim}
cat /etc/bacula/changer.volumes
\end{bVerbatim}

Be sure to include a full path to the file, which can have any name. The
contents of the file must be of the following format:

\begin{bVerbatim}
1:Volume1
2:Volume2
3:Volume3
...
\end{bVerbatim}

Where the 1, 2, 3 are the slot numbers and Volume1, Volume2, \ldots{} are the
Volume names in those slots. You can have multiple files that represent the
Volumes in different magazines, and when you change magazines, simply copy the
contents of the correct file into your \bfilename{/etc/bacula/changer.volumes} file.
There is no need to stop and start \bacula{} when you change magazines, simply
put the correct data in the file, then run the \bcommandname{update slots} command, and
your autochanger will appear to \bacula{} to be an autochanger with barcodes.
\label{blb:updateslots}

\section{The Full Form of the Update Slots Command}
\index[general]{Full Form of the Update Slots Command}
\index[general]{Command!Full Form of the Update Slots}

If you change only one cartridge in the magazine, you may not want to scan all
Volumes, so the \bcommandname{update slots} command (as well as the
\bcommandname{update slots scan} command) has the additional form:

\begin{bVerbatim}
update slots=n1,n2,n3-n4, ...
\end{bVerbatim}

where the keyword \textbf{scan} can be appended or not. The n1,n2, \ldots{} represent
Slot numbers to be updated and the form n3-n4 represents a range of Slot
numbers to be updated (e.g. 4-7 will update Slots 4,5,6, and 7).

This form is particularly useful if you want to do a scan (time expensive) and
restrict the update to one or two slots.

For example, the command:

\begin{bVerbatim}
update slots=1,6 scan
\end{bVerbatim}

will cause \bacula{} to load the Volume in Slot 1, read its Volume label and
update the Catalog. It will do the same for the Volume in Slot 6. The command:


\begin{bVerbatim}
update slots=1-3,6
\end{bVerbatim}

will read the barcoded Volume names for slots 1,2,3 and 6 and make the
appropriate updates in the Catalog. If you don't have a barcode reader or have
not modified the \mtxchanger{} script as described above, the above command will
not find any Volume names so will do nothing.
\label{blb:FreeBSD}

\section{FreeBSD Issues}
\index[general]{Issues!FreeBSD}
\index[general]{FreeBSD Issues}

If you are having problems on FreeBSD when \bacula{} tries to select a tape, and
the message is \textbf{Device not configured}, this is because FreeBSD has made
the tape device \bfilename{/dev/nsa1} disappear when there is no tape mounted in the
autochanger slot. As a consequence, \bacula{} is unable to open the device. The
solution to the problem is to make sure that some tape is loaded into the tape
drive before starting \bacula{}. This problem is corrected in \bacula{} versions
1.32f-5 and later.

Please see the
\bxlink{Tape Testing}{blb:FreeBSDTapes}{problems}{chapter} of the \bproblemman{} for
\textbf{important} information concerning your tape drive before doing the
autochanger testing.
\label{blb:AutochangerTesting}

\section{Testing Autochanger and Adapting mtx-changer script}
\index[general]{Testing the Autochanger}
\index[general]{Adapting Your mtx-changer script}


Before attempting to use the autochanger with \bacula{}, it is preferable to
``hand-test'' that the changer works. To do so, we suggest you do the
following commands (assuming that the \mtxchanger{} script is installed in
\bfilename{/opt/bacula/scripts/mtx-changer}):

\begin{bdescription}

\item [Make sure \bacula{} is not running.]

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ list \ 0 \ /dev/nst0 \ 0]
\index[sd]{mtx-changer list}

This command should print:

\begin{bVerbatim}
   1:
   2:
   3:
   ...
\end{bVerbatim}

or one number per line for each slot that is  occupied in your changer, and
the number should be  terminated by a colon (\textbf{:}). If your changer has
barcodes, the barcode will follow the colon.  If an error message is printed,
you must resolve the  problem (e.g. try a different \acs{SCSI} control device name
if \bfilename{/dev/sg0}  is incorrect). For example, on FreeBSD systems, the
autochanger  \acs{SCSI} control device is generally \bfilename{/dev/pass2}.

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ listall \ 0 \ /dev/nst0 \ 0]
\index[sd]{mtx-changer listall}

This command should print:

\begin{bVerbatim}
 Drive content:         D:Drive num:F:Slot loaded:Volume Name
 D:0:F:2:vol2        or D:Drive num:E
 D:1:F:42:vol42
 D:3:E

 Slot content:
 S:1:F:vol1             S:Slot num:F:Volume Name
 S:2:E               or S:Slot num:E
 S:3:F:vol4

 Import/Export tray slots:
 I:10:F:vol10           I:Slot num:F:Volume Name
 I:11:E              or I:Slot num:E
 I:12:F:vol40
\end{bVerbatim}

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ transfer \ 1 \ 2]
\index[sd]{mtx-changer listall}

This command should transfer a volume from source (1) to destination (2)

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ slots ]
\index[sd]{mtx-changer slots}

This command should return the number of slots in your autochanger.

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ unload \ 1 \ /dev/nst0 \ 0 ]
\index[sd]{mtx-changer unload}

   If a tape is loaded from slot 1, this should cause it to be unloaded.

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ load \ 3 \ /dev/nst0 \ 0 ]
\index[sd]{mtx-changer load}

Assuming you have a tape in slot 3,  it will be loaded into drive (0).


\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ loaded \ 0 \ /dev/nst0 \ 0]
\index[sd]{mtx-changer loaded}

It should print ``3''
Note, we have used an ``illegal'' slot number 0. In this case, it is simply
ignored because the slot number is not used.  However, it must be specified
because the drive parameter at the end of the command is needed to select
the correct drive.

\item [/opt/bacula/scripts/mtx-changer \ /dev/sg0 \ unload \ 3 /dev/nst0 \ 0]

will unload the tape into slot 3.

\end{bdescription}

Once all the above commands work correctly, assuming that you have the right
\textbf{\bdirectivename{Changer Command}} in your configuration, \bacula{} should be able to operate
the changer. The only remaining area of problems will be if your autoloader
needs some time to get the tape loaded after issuing the command. After the
\mtxchanger{} script returns, \bacula{} will immediately rewind and read the
tape. If \bacula{} gets rewind I/O errors after a tape change, you will probably
need to insert a \textbf{sleep 20} after the \mtx{} command, but be careful to
exit the script with a zero status by adding \textbf{exit 0} after any additional
commands you add to the script. This is because \bacula{} checks the return
status of the script, which should be zero if all went well.

You can test whether or not you need a \textbf{sleep} by putting the following
commands into a file and running it as a script:

\begin{bVerbatim}
#!/bin/sh
/opt/bacula/scripts/mtx-changer /dev/sg0 unload 1 /dev/nst0 0
/opt/bacula/scripts/mtx-changer /dev/sg0 load 3 /dev/nst0 0
mt -f /dev/st0 rewind
mt -f /dev/st0 weof
\end{bVerbatim}

If the above script runs, you probably have no timing problems. If it does not
run, start by putting a \textbf{sleep 30} or possibly a \textbf{sleep 60} in the
script just after the \mtxchanger{} load command. If that works, then you should
move the sleep into the actual \mtxchanger{} script so that it will be
effective when \bacula{} runs.

A second problem that comes up with a small number of autochangers is that
they need to have the cartridge ejected before it can be removed. If this is
the case, the \textbf{load 3} will never succeed regardless of how long you wait.
If this seems to be your problem, you can insert an eject just after the
unload so that the script looks like:

\begin{bVerbatim}
#!/bin/sh
/opt/bacula/scripts/mtx-changer /dev/sg0 unload 1 /dev/nst0 0
mt -f /dev/st0 offline
/opt/bacula/scripts/mtx-changer /dev/sg0 load 3 /dev/nst0 0
mt -f /dev/st0 rewind
mt -f /dev/st0 weof
\end{bVerbatim}

Obviously, if you need the \bcommandname{offline} command, you should move it into the
\mtxchanger{} script ensuring that you save the status of the \mtx{} command
or always force an \textbf{exit 0} from the script, because \bacula{} checks the
return status of the script.

As noted earlier, there are several scripts in \textbf{\bdirectoryname{\bbracket{bacula-source}/examples/devices}}
that implement the above features,
so they may be a help to you in getting your script to work.

If \bacula{} complains ``Rewind error on \bfilename{/dev/nst0}. ERR=Input/output error.'' you
most likely need more sleep time in your \mtxchanger{} before returning to
\bacula{} after a load command has been completed.

\label{blb:using}

\section{Using the Autochanger}
\index[general]{Using the Autochanger}
\index[general]{Autochanger!Using the}

Let's assume that you have properly defined the necessary Storage daemon
Device records, and you have added the \textbf{\bdirectivename{Autochanger = yes}} record to the
Storage resource in your Director's configuration file.

Now you fill your autochanger with say six blank tapes.

What do you do to make \bacula{} access those tapes?

One strategy is to prelabel each of the tapes. Do so by starting \bacula{}, then
with the Console program, enter the \bcommandname{label} command:

\begin{bVerbatim}
./bconsole
Connecting to Director rufus:9101
1000 OK: rufus-dir Version: 1.26 (4 October 2002)
*label
\end{bVerbatim}

it will then print something like:

\begin{bVerbatim}
Using default Catalog name=BackupDB DB=bacula
The defined Storage resources are:
     1: Autochanger
     2: File
Select Storage resource (1-2): 1
\end{bVerbatim}

I select the autochanger (1), and it prints:

\begin{bVerbatim}
Enter new Volume name: TestVolume1
Enter slot (0 for none): 1
\end{bVerbatim}

where I entered \textbf{TestVolume1} for the tape name, and slot \textbf{1} for the
slot. It then asks:

\begin{bVerbatim}
Defined Pools:
     1: Default
     2: File
Select the Pool (1-2): 1
\end{bVerbatim}

I select the Default pool. This will be automatically done if you only have a
single pool, then \bacula{} will proceed to unload any loaded volume, load the
volume in slot 1 and label it. In this example, nothing was in the drive, so
it printed:

\begin{bVerbatim}
Connecting to Storage daemon Autochanger at localhost:9103 ...
Sending label command ...
3903 Issuing autochanger "load slot 1" command.
3000 OK label. Volume=TestVolume1 Device=/dev/nst0
Media record for Volume=TestVolume1 successfully created.
Requesting mount Autochanger ...
3001 Device /dev/nst0 is mounted with Volume TestVolume1
You have messages.
*
\end{bVerbatim}

You may then proceed to label the other volumes. The messages will change
slightly because \bacula{} will unload the volume (just labeled TestVolume1)
before loading the next volume to be labeled.

Once all your Volumes are labeled, \bacula{} will automatically load them as they
are needed.

To ``see'' how you have labeled your Volumes, simply enter the \bcommandname{list volumes} command from the Console program, which should print something like
the following:

\begin{bVerbatim}
  * list volumes
Using default Catalog name=BackupDB DB=bacula
Defined Pools:
     1: Default
     2: File
Select the Pool (1-2): 1
+-------+----------+--------+---------+-------+--------+----------+-------+------+
| MedId | VolName  | MedTyp | VolStat | Bites | LstWrt | VolReten | Recyc | Slot |
+-------+----------+--------+---------+-------+--------+----------+-------+------+
| 1     | TestVol1 | DDS-4  | Append  | 0     | 0      | 30672000 | 0     | 1    |
| 2     | TestVol2 | DDS-4  | Append  | 0     | 0      | 30672000 | 0     | 2    |
| 3     | TestVol3 | DDS-4  | Append  | 0     | 0      | 30672000 | 0     | 3    |
| ...                                                                            |
+-------+----------+--------+---------+-------+--------+----------+-------+------+
\end{bVerbatim}

\label{blb:Barcodes}

\section{Barcode Support}
\index[general]{Support!Barcode}
\index[general]{Barcode Support}

\bacula{} provides barcode support with two Console commands, \bcommandname{label barcodes}
and \bcommandname{update slots}.

The \bcommandname{label barcodes} will cause \bacula{} to read the barcodes of all the
cassettes that are currently installed in the magazine (cassette holder) using
the \mtxchanger{} \textbf{list} command. Each cassette is mounted in turn and
labeled with the same Volume name as the barcode.

The \bcommandname{update slots} command will first obtain the list of cassettes and
their barcodes from \mtxchanger{}. Then it will find each volume in turn
in the catalog database corresponding to the barcodes and set its slot to
correspond to the value just read. If the Volume is not in the catalog, then
nothing will be done. This command is useful for synchronizing \bacula{} with the
current magazine in case you have changed magazines or in case you have moved
cassettes from one slot to another. If the autochanger is empty, nothing will
be done.

The \textbf{\bdirectivename{Cleaning Prefix}} statement can be used in the Pool resource to define
a Volume name prefix, which if it matches that of the Volume (barcode) will
cause that Volume to be marked with a VolStatus of \textbf{Cleaning}. This will
prevent \bacula{} from attempting to write on the Volume.

\section{Use bconsole to display Autochanger content}

The \bcommandname{status slots storage=xxx} command displays autochanger content.

\begin{bVerbatim}
 Slot |  Volume Name    |  Status  |      Type         |    Pool        |  Loaded |
------+-----------------+----------+-------------------+----------------+---------|
    1 |           00001 |   Append |  DiskChangerMedia |        Default |    0    |
    2 |           00002 |   Append |  DiskChangerMedia |        Default |    0    |
    3*|           00003 |   Append |  DiskChangerMedia |        Scratch |    0    |
    4 |                 |          |                   |                |    0    |
\end{bVerbatim}

If you see a \textbf{*} near the slot number, you have to run \bcommandname{update slots}
command to synchronize autochanger content with your catalog.

\label{blb:interface}

\section{\bacula{} Autochanger Interface}
\index[general]{Interface!\bacula{} Autochanger}
\index[general]{\bacula{} Autochanger Interface}

\bacula{} calls the autochanger script that you specify on the \textbf{\bdirectivename{Changer Command}}
statement. Normally this script will be the \mtxchanger{} script
that we provide, but it can in fact be any program. The only requirement
for the script is that it must understand the commands that
\bacula{} uses, which are \textbf{loaded}, \textbf{load},
\textbf{unload}, \textbf{list}, and \textbf{slots}. In addition,
each of those commands must return the information in the precise format as
specified below:

Currently the changer commands used are:
\begin{bdescription}
\item [loaded] returns number of the slot that is loaded, base 1,
  in the drive or 0 if the drive is empty.
\item [load] loads a specified slot (note, some autochangers
  require a 30 second pause after this command) into
  the drive.
\item [unload] unloads the device (returns cassette to its slot).
\item [list] returns one line for each cassette in the autochanger
  in the format \bbracket{slot}:\bbracket{barcode}. Where
  the \bbracket{slot} is the non-zero integer representing
  the slot number, and \bbracket{barcode} is the barcode
  associated with the cassette if it exists and if you
  autoloader supports barcodes. Otherwise the barcode
  field is blank.
\item [slots] returns total number of slots in the autochanger.
\end{bdescription}

\bacula{} checks the exit status of the program called, and if it is zero, the
data is accepted. If the exit status is non-zero, \bacula{} will print an
error message and request the tape be manually mounted on the drive.
