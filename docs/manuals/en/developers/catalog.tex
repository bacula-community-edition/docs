\chapter{Catalog Services}
\label{blb:TheChapterStart30}
\label{blb:developers:catalog:service}
\index[general]{Services!Catalog}
\index[general]{Catalog Services}

\section{General}
\index[general]{General}

This chapter is intended to be a technical discussion of the Catalog services
and as such is not targeted at end users but rather at developers and system
administrators that want or need to know more of the working details of \textbf{\bacula{}}.

The \textbf{\bacula{} Catalog} services consist of the programs that provide the \acs{SQL}
database engine for storage and retrieval of all information concerning files
that were backed up and their locations on the storage media.

We have investigated the possibility of using the following \acs{SQL} engines for
\bacula{}: Beagle, mSQL, \acs{GNU} \acs{SQL}, \postgresql{}, Oracle, and \mysql{}. Each
presents certain problems with either licensing or maturity. At present, we
have chosen for development purposes to use \mysql{} and \postgresql{}.
\mysql{} was chosen because it is fast, proven to be reliable, widely used, and
actively being developed. \mysql{} is released under the \acs{GNU} \acs{GPL} license.
\postgresql{} was chosen because it is a full-featured, very mature database, and
because Dan Langille did the \bacula{} driver for it. \postgresql{} is distributed
under the BSD license.

The \bacula{} \acs{SQL} code has been written in a manner that will allow it to be
easily modified to support any of the current \acs{SQL} database systems on the
market (for example: mSQL, iODBC, unixODBC, Solid, OpenLink ODBC, EasySoft
ODBC, InterBase, Oracle8, Oracle7, and DB2).

If you do not specify either \texttt{{-}{-}with-mysql} or \texttt{{-}{-}with-postgresq} on the \btool{./configure} line, \bacula{} will use its minimalist
internal database. This database is kept for build reasons but is no longer
supported. \bacula{} \textbf{requires} one of the three databases (\mysql{},
\postgresql{}, or \sqlite{}) to run.

\subsection{Filenames and Maximum Filename Length}
\index[general]{Filenames and Maximum Filename Length}
\index[general]{Length!Filenames and Maximum Filename}

In general, either \mysql{} or \postgresql{} permit storing arbitrary long
path names and file names in the catalog database. In practice, there still
may be one or two places in the Catalog interface code that restrict the
maximum path length to 512 characters and the maximum file name length to 512
characters. These restrictions are believed to have been removed. Please note,
these restrictions apply only to the Catalog database and thus to your ability
to list online the files saved during any job. All information received and
stored by the Storage daemon (normally on tape) allows and handles arbitrarily
long path and filenames.

\subsection{Installing and Configuring \mysql{}}
\index[general]{\mysql{}!Installing and Configuring}
\index[general]{Installing and Configuring \mysql{}}

For the details of installing and configuring \mysql{}, please see the
\bxlink{Installing and Configuring \mysql{}}{blb:MySqlChapter}{main}{chapter} of
the \bmainman{}.

\subsection{Installing and Configuring \postgresql{}}
\index[general]{\postgresql{}!Installing and Configuring}
\index[general]{Installing and Configuring \postgresql{}}

For the details of installing and configuring \postgresql{}, please see the
\bxlink{Installing and Configuring \postgresql{}}{blb:PostgreSqlChapter}{main}{chapter}
 of the \bmainman{}.

\subsection{Internal \bacula{} Catalog}
\index[general]{Catalog!Internal \bacula{}}
\index[general]{Internal \bacula{} Catalog}

Please see the \bxlink{Internal \bacula{} Database}{blb:chap:InternalBaculaDatabase}{misc}{chapter}
of the \bmiscman{} for more details.

\subsection{Database Table Design}
\index[general]{Design!Database Table}
\index[general]{Database Table Design}

All discussions that follow pertain to the \mysql{} database. The details for the
\postgresql{} databases are quite similar.

Because the Catalog database may contain very large amounts of data for large
sites, we have made a modest attempt to normalize the data tables to reduce
redundant information. While reducing the size of the database significantly,
it does, unfortunately, add some complications to the structures.

In simple terms, the Catalog database must contain a record of all Jobs run by
\bacula{}, and for each Job, it must maintain a list of all files saved, with
their File Attributes (permissions, create date, ...), and the location and
Media on which the file is stored. This is seemingly a simple task, but it
represents a huge amount interlinked data. Note: the list of files and their
attributes is not maintained when using the internal \bacula{} database. The data
stored in the File records, which allows the user or administrator to obtain a
list of all files backed up during a job, is by far the largest volume of
information put into the Catalog database.

Although the Catalog database has been designed to handle backup data for
multiple clients, some users may want to maintain multiple databases, one for
each machine to be backed up. This reduces the risk of confusion of accidental
restoring a file to the wrong machine as well as reducing the amount of data
in a single database, thus increasing efficiency and reducing the impact of a
lost or damaged database.

\section{Sequence of Creation of Records for a Save Job}
\index[general]{Sequence of Creation of Records for a Save Job}
\index[general]{Job!Sequence of Creation of Records for a Save}

Start with StartDate, ClientName, Filename, Path, Attributes, MediaName,
MediaCoordinates. (PartNumber, NumParts). In the steps below, ``Create new''
means to create a new record whether or not it is unique. ``Create unique''
means each record in the database should be unique. Thus, one must first
search to see if the record exists, and only if not should a new one be
created, otherwise the existing RecordId should be used.

\begin{benumerate}
\item Create new Job record with StartDate; save JobId
\item Create unique Media record; save MediaId
\item Create unique Client record; save ClientId
\item Create unique Filename record; save FilenameId
\item Create unique Path record; save PathId
\item Create unique Attribute record; save AttributeId
   store ClientId, FilenameId, PathId, and Attributes
\item Create new File record
   store JobId, AttributeId, MediaCoordinates, etc
\item Repeat steps 4 through 8 for each file
\item Create a JobMedia record; save MediaId
\item Update Job record filling in EndDate and other Job statistics
   \end{benumerate}

\section{Database Tables}
\index[general]{Database Tables}
\index[general]{Tables!Database}
\LTXtable{\linewidth}{t-dev-dbpath}

The \bilink{\btable{Path} table}{blb:dev:path:table:layout} contains the path or directory names of all
directories on the system or systems. The filename and any MSDOS disk name are
stripped off. As with the filename, only one copy of each directory name is
kept regardless of how many machines or drives have the same directory. These
path names should be stored in Unix path name format.

Some simple testing on a Linux file system indicates that separating the
filename and the path may be more complication than is warranted by the space
savings. For example, this system has a total of 89,097 files, 60,467 of which
have unique filenames, and there are 4,374 unique paths.

Finding all those files and doing two stats() per file takes an average wall
clock time of 1 min 35 seconds on a 400MHz machine running RedHat 6.1 Linux.

Finding all those files and putting them directly into a \mysql{} database with
the path and filename defined as TEXT, which is variable length up to 65,535
characters takes 19 mins 31 seconds and creates a 27.6 MByte database.

Doing the same thing, but inserting them into Blob fields with the filename
indexed on the first 30 characters and the path name indexed on the 255 (max)
characters takes 5 mins 18 seconds and creates a 5.24 MB database. Rerunning
the job (with the database already created) takes about 2 mins 50 seconds.

Running the same as the last one (Path and Filename Blob), but Filename
indexed on the first 30 characters and the Path on the first 50 characters
(linear search done there after) takes 5 mins on the average and creates a 3.4
MB database. Rerunning with the data already in the DB takes 3 mins 35
seconds.

Finally, saving only the full path name rather than splitting the path and the
file, and indexing it on the first 50 characters takes 6 mins 43 seconds and
creates a 7.35 MB database.


\LTXtable{\linewidth}{t-dev-dbfile}

The \bilink{\btable{File} table}{blb:dev:file:table:layout} contains one entry for each file backed up by
\bacula{}. Thus a file that is backed up multiple times (as is normal) will have
multiple entries in the File table. This will probably be the table with the
most number of records. Consequently, it is essential to keep the size of this
record to an absolute minimum. At the same time, this table must contain all
the information (or pointers to the information) about the file and where it
is backed up. Since a file may be backed up many times without having changed,
the path is stored in separate tables.

This table contains by far the largest amount of information in the Catalog
database, both from the stand point of number of records, and the stand point
of total database size. As a consequence, the user must take care to
periodically reduce the number of File records using the \bcommandname{retention}
command in the Console program.

\LTXtable{\linewidth}{t-dev-dbjob}

The \bilink{\btable{Job} table}{blb:dev:job:table:layout} contains one record for each Job run by \bacula{}. Thus
normally, there will be one per day per machine added to the database. Note,
the JobId is used to index Job records in the database, and it often is shown
to the user in the Console program. However, care must be taken with its use
as it is not unique from database to database. For example, the user may have
a database for Client data saved on machine Rufus and another database for
Client data saved on machine Roxie. In this case, the two database will each
have JobIds that match those in another database. For a unique reference to a
Job, see Job below.

The Name field of the Job record corresponds to the Name resource record given
in the Director's configuration file. Thus it is a generic name, and it will
be normal to find many Jobs (or even all Jobs) with the same Name.

The Job field contains a combination of the Name and the schedule time of the
Job by the Director. Thus for a given Director, even with multiple Catalog
databases, the Job will contain a unique name that represents the Job.

For a given Storage daemon, the VolSessionId and VolSessionTime form a unique
identification of the Job. This will be the case even if multiple Directors
are using the same Storage daemon.

The Job Type (or simply Type) can have one of the following values:

\LTXtable{\linewidth}{t-dev-dbjobtypes}

Note, the Job Type values in table \vref{blb:dev:jobtypes} noted above are not kept in an \acs{SQL} table.


The JobStatus field specifies how the job terminated, and can be one of the
following:
\LTXtable{\linewidth}{t-dev-dbjobstatuses}


\LTXtable{\linewidth}{t-dev-dbfileset}

The \bilink{\btable{FileSet} table}{blb:dev:filesets:table:layout} contains one entry for each FileSet that is used. The
MD5 signature is kept to ensure that if the user changes anything inside the
FileSet, it will be detected and the new FileSet will be used. This is
particularly important when doing an incremental update. If the user deletes a
file or adds a file, we need to ensure that a Full backup is done prior to the
next incremental.


\LTXtable{\linewidth}{t-dev-dbjobmedia}

The \bilink{\btable{JobMedia} table}{blb:dev:jobmedia:table:layout} contains one entry at the following: start of
the job, start of each new tape file, start of each new tape, end of the
job.  Since by default, a new tape file is written every 2GB, in general,
you will have more than 2 JobMedia records per Job.  The number can be
varied by changing the "Maximum File Size" specified in the Device
resource.  This record allows \bacula{} to efficiently position close to
(within 2GB) any given file in a backup.  For restoring a full Job,
these records are not very important, but if you want to retrieve
a single file that was written near the end of a 100GB backup, the
JobMedia records can speed it up by orders of magnitude by permitting
forward spacing files and blocks rather than reading the whole 100GB
backup.




\LTXtable{\linewidth}{t-dev-dbmedia}

The \bilink{\btable{Volume} table}{blb:dev:media:table:layout}\footnote{Internally referred to as the Media table}  contains
one entry for each volume, that is each tape, cassette (8mm, DLT, DAT, ...),
or file on which information is or was backed up. There is one Volume record
created for each of the NumVols specified in the Pool resource record.

\LTXtable{\linewidth}{t-dev-dbpool}

The \bilink{\btable{Pool} table}{blb:dev:pool:table:layout} contains one entry for each media pool controlled by
\bacula{} in this database. One media record exists for each of the NumVols
contained in the Pool. The PoolType is a \bacula{} defined keyword. The MediaType
is defined by the administrator, and corresponds to the MediaType specified in
the Director's Storage definition record. The CurrentVol is the sequence
number of the Media record for the current volume.


\LTXtable{\linewidth}{t-dev-dbclient}

The \bilink{\btable{Client} table}{blb:dev:client:table:layout} contains one entry for each machine backed up by \bacula{}
in this database. Normally the Name is a fully qualified domain name.


\LTXtable{\linewidth}{t-dev-dbstorage}

The \bilink{\btable{Storage} table}{blb:dev:storage:table:layout} contains one entry for each Storage used.


\LTXtable{\linewidth}{t-dev-dbcounter}

The \bilink{\btable{Counter} table}{blb:dev:counter:table:layout} contains one entry for each permanent counter defined
by the user.

\LTXtable{\linewidth}{t-dev-dbjobhistory}

The \bilink{\btable{JobHisto} table}{blb:dev:jobhistory:table:layout} is the same as the Job table, but it keeps
long term statistics (i.e. it is not pruned with the Job).


\LTXtable{\linewidth}{t-dev-dblog}

The \bilink{\btable{Log} table}{blb:dev:log:table:layout} contains a log of all Job output.

\LTXtable{\linewidth}{t-dev-dblocation}

The \bilink{\btable{Location} table}{blb:dev:location:table:layout} defines where a Volume is physically.


\LTXtable{\linewidth}{t-dev-dblocationlog}

The \bilink{\btable{Location Log} table}{blb:dev:locationlog:table:layout} contains a log of all Job output.


\LTXtable{\linewidth}{t-dev-dbversion}

The \bilink{\btable{Version} table}{blb:dev:version:table:layout} defines the \bacula{} database version number. \bacula{}
checks this number before reading the database to ensure that it is compatible
with the \bacula{} binary file.


\LTXtable{\linewidth}{t-dev-dbbasefiles}

The \bilink{\btable{BaseFiles} table}{blb:dev:basefiles:table:layout} contains all the File references for a particular
JobId that point to a Base file -- i.e. they were previously saved and hence
were not saved in the current JobId but in BaseJobId under FileId. FileIndex
is the index of the file, and is used for optimization of Restore jobs to
prevent the need to read the FileId record when creating the in memory tree.
This record is not yet implemented.
