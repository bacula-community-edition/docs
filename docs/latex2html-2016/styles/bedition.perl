# bedition.perl
#    by Philippe Chauvat <philippe@chauvat.eu>
# This file should be the only one different between Enterprise and Community
# ====================================================================== 
sub do_cmd_bedition {
    print "PCT: bedition\n" if ($VERBOSITY > 0) ;
    join(' ',&do_cmd_beditionname(),'Edition',$_[0]) ;
}
sub do_cmd_beditionname {
    print "PCT: do_cmd_beditionname\n" if ($VERBOSITY > 0) ;
    return 'Enterprise' ;
}
# ====================================================================== 
1;
