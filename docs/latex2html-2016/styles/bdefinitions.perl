# bdefinitions.perl
#    by Philippe Chauvat <philippe@chauvat.eu>
# This file is designed to support the builing of Bacula (http://www.bacula.org) and/or
# Bacula Enterprise Edition (http://www.baculasystems.com) manuals.
# Support of the bdefinitions.sty Bacula package
# ======================================================================
# Does not require any package => do_require_package('') ;



# Package Options
#No package options
#sub do_bdefinitions_option {}

# ======================================================================
sub bnext_arg { # Modifies $_
    #
    # This is a whole duplicate of the x_next_arg function
    # defined in the graphics-support.perl file.
    # I renamed it to be:
    # a) consistent with the other definitions
    # b) be sure there will be no conflict with anything else.
    my $arg;
    &missing_braces unless ((s/$next_pair_pr_rx/$arg=$2;''/e)
			    ||(s/$next_pair_rx/$arg=$2;''/e));
    $arg;
}

sub do_cmd_bfilename {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('','','','','','bfilename',@_) ;
}
sub bfilename {
    local($btext)= @_ ;
    join('','<span class="bfilename">', $btext, '</span>') ;
}

sub do_cmd_btable {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('','','','','','btable',@_) ;
}
sub btable {
    local($btext)= @_ ;
    join('','<span class="btable">', $btext, '</span>') ;
}

sub do_cmd_bdirectoryname {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('','','','','','bdirectoryname',@_) ;
}
sub bdirectoryname {
    local($btext)= @_ ;
    join('','<span class="bdirectoryname">', $btext, '</span>') ;
}

sub do_cmd_bdirectory {
    local($_)=@_;
    my $file = &bnext_arg() ;
     &styled_text_chunk('','','','','','bdirectory',@_) ;
}
sub bdirectory {
    local($btext)= @_ ;
    join(' ',eval("&bdirectoryname(\$btext)"),'directory') ;
}

sub do_cmd_bdirectivename {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('','','','','','bdirectivename',@_) ;
}
sub bdirectivename {
    local($btext)=@_;
    join('','<span class="bdirectivename">', $btext, '</span>') ;
}
sub do_cmd_bdirective {
    local($_)=@_;
    my $file = &bnext_arg() ;
    &styled_text_chunk('','','','','','bdirective',@_) ;
}
sub bdirective {
    local($btext)= @_ ;
    join(' ',eval("&bdirectivename(\$btext)"),'directive') ;
}

sub do_cmd_bPool {
    join('','<span class="bresourcename">','Pool','</span>',$_[0]) ;
}
sub do_cmd_bConsole {
    join('','<span class="bresourcename">','Console','</span>',$_[0]) ;
}
sub do_cmd_bCatalog {
    join('','<span class="bresourcename">','Catalog','</span>',$_[0]) ;
}
sub do_cmd_bJob     {
    join('','<span class="bresourcename">','Job','</span>',$_[0]) ;
}
sub do_cmd_bVolume  {
    join('','<span class="bresourcename">','Volume','</span>',$_[0]) ;
}
sub do_cmd_bDevice  {
    join('','<span class="bresourcename">','Device','</span>',$_[0]) ;
}
sub do_cmd_bFileSet {
    join('','<span class="bresourcename">','FileSet','</span>',$_[0]) ;
}
sub do_cmd_bStorage {
    join('','<span class="bresourcename">','Storage','</span>',$_[0]) ;
}
sub do_cmd_bJobId {
    join('','<span class="bresourcename">','JobId','</span>',$_[0]) ;
}
sub do_cmd_bClient {
    join('','<span class="bresourcename">','Client','</span>',$_[0]) ;
}
sub do_cmd_bDirector {
    join('','<span class="bdaemon">','Director','</span>',$_[0]) ;
}
sub do_cmd_bconsolename {
    join('','<span class="btool">','bconsole','</span>',$_[0]) ;
}
sub do_cmd_bconsole {
    join('',bbacula(),'<span class="bcommandname">',' ','Console','</span>',$_[0]) ;
}
sub do_cmd_bstructurename {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('','','','','','bstructurename',@_) ;
}
sub bstructurename {
    local($btext)= @_ ;
    join('','<span class="bstructurename">',$btext,'</span>') ;
}
sub do_cmd_bstructure {
    local($_)=@_;
    my $mybstructure = &bnext_arg() ;
    &styled_text_chunk('','','','','','bstructure',@_) ;
}
sub bstructure {
    local($btext)= @_ ;
    join(' ',eval("&bstructurename(\$btext)"),'structure') ;
}
sub do_cmd_bresourcename {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('SPAN','CLASS="bresourcename"','','','','',@_) ;
}
sub bresourcename {
    local($mybresource) = @_ ;
    join('','<span class="bresourcename">',$mybresource,'</span>') ;
}
sub bstatus {
    local($mybstatus) = @_ ;
    join('','<span class="bstatus">',$mybstatus,'</span>') ;
}
sub bjobtype {
    local($mybjobtype) = @_ ;
    join('','<span class="bjobtype">',$mybjobtype,'</span>') ;
}
sub do_cmd_bresource {
    local($_)=@_;
    my $file = &bnext_arg() ;
    &styled_text_chunk('','','','','','bresource',@_) ;
}
sub bresource {
    local($btext)= @_ ;
    join(' ',eval("&bresourcename(\$btext)"),'resource') ;
}
sub do_cmd_bresources {
    local($_)=@_;
    my $file = &bnext_arg() ;
    &styled_text_chunk('','','','','','bresources',@_) ;
}
sub bresources {
    local($btext)= @_ ;
    join('',eval("&bresource(\$btext)"),'s') ;
}
sub do_cmd_bresourceq {
    local($_)=@_;
    my $file = &bnext_arg() ;
    &styled_text_chunk('','','','','','bresourceq',@_) ;
}
sub bresourceq {
    local($btext)= @_ ;
    $btext = join('',$btext,"'s") ;
    join(' ',eval("&bresourcename(\$btext)"),'resource') ;
}
sub do_cmd_bcommandname {
    local($_)=@_;
    my $file = &bnext_arg();
    &styled_text_chunk('','','','','','bcommandname',@_) ;
}
sub bcommandname {
    local($mybcommand) = @_ ;
    join('','<span class="bcommandname">',$mybcommand,'</span>') ;
}
sub do_cmd_bcommand {
    local($_)=@_;
    my $file = &bnext_arg() ;
    &styled_text_chunk('','','','','','bcommand',@_) ;
}
sub bcommand {
    local($btext)= @_ ;
    join('',eval("&bcommandname(\$btext)"),' command') ;
}
sub do_cmd_btool {
    local($_)=@_;
    my $tool = &bnext_arg() ;
    &styled_text_chunk('','','','','','btool',@_) ;
}
sub btool {
    local($btext) = @_;
    print "PCT: btool: text: $text\n" if ($VERBOSITY > 10) ;
    join('','<span class="btool">',$btext,'</span>') ;
}
sub do_cmd_bbracket {
    local($_)=@_;
    my $bracket = &bnext_arg() ;
    &styled_text_chunk('','','','','','bbracket',@_) ;
}
sub bbracket {
    local($btext)= @_ ;
    join('','<span class="bbracket">&lt;',$btext,'&gt;</span>') ;
}
sub do_cmd_bvalue {
    local($_)=@_;
    my $value = &bnext_arg();
    &styled_text_chunk('','','','','','bvalue',@_) ;
}
sub bvalue {
    local($btext) = @_;
    join('','<span class="bvalue">',$btext,'</span>') ;
}
sub do_cmd_bhighlight {
    local($_)=@_;
    my $value = &bnext_arg();
    &styled_text_chunk('','','','','','bhighlight',@_) ;
}
sub bhighlight {
    local($btext) = @_;
    join('','<span class="bhighlight">',$btext,'</span>') ;
}
sub do_cmd_bkeyword {
    local($_)=@_;
    my $value = &bnext_arg();
    &styled_text_chunk('','','','','','bkeyword',@_) ;
}
##
## bkeyword is defined the same as bvalue
sub bkeyword {
    local($btext) = @_;
    join('','<span class="bvalue">',$btext,'</span>') ;
}
sub do_cmd_bdefaultvalue {
    local($_)=@_;
    my $value = &bnext_arg();
    &styled_text_chunk('','','','','','bdefaultvalue',@_) ;
}
sub bdefaultvalue {
    local($btext) = @_;
    join('','<span class="bdefaultvalue">',$btext,'</span>') ;
}
sub do_cmd_bacula {
    local($_)=@_;
    my $bbacula = &bnext_arg();
    &styled_text_chunk('','','','','','bbacula',@_) ;
}
sub bbacula {
    local($_)=@_;
    join('','<span class="bbacula">','Bacula','</span>') ;
}
sub do_cmd_baculasystems{
    join('','<a class="baculasystems" href="http://www.baculasystems.com">','Bacula Systems','</a>',$_[0]);
}
sub do_cmd_bweb{
    join('','<span class="bweb">BWeb</span>',$_[0]);
}
sub do_cmd_mssql{
    join('','<span class="bsqltool">MSSQL</span>',$_[0]);
}
sub do_cmd_postgresql {
    join('','<span class="bsqltool">PostgreSQL</span>',$_[0]);
}
sub do_cmd_mysql {
    join('','<span class="bsqltool">MySQL</span>',$_[0]);
}
sub do_cmd_sqlite {
    join('','<span class="bsqltool">SQLite</span>',$_[0]);
}

sub do_cmd_bnextpage {
    # We don't do nothing with it..
}
sub do_cmd_byesorno {
    join('','<span class="byesorno">&lt;yes|no&gt;</span>',$_[0]);
}
sub do_cmd_bzeroone {
    join('','<span class="bzeroone">&lt;0|1&gt;</span>',$_[0]);
}
sub do_cmd_gt {
    join('',"&gt;",$_[0]);
}
sub do_cmd_lt {
    join('',"&lt;",$_[0]);
}
#
# Manual names
sub buildmanualname {
    local($mymanual) = @_ ;
    my $bacula = bbacula() ;
    my $bedition = &do_cmd_beditionname() ;
    join('','<span class="bmanualname">',$bacula,' ',$bedition,' ',$mymanual,'</span>');
}
sub do_cmd_bmainman {
    $mybuild = buildmanualname("Main manual") ;
    join('',$mybuild,$_[0]);
}
sub do_cmd_bconsoleman {
    $mybuild = buildmanualname("Console manual") ;
    join('',$mybuild,$_[0]);
}
sub do_cmd_bdevelman {
    $mybuild = buildmanualname("Developer's manual") ;
    join('',$mybuild,$_[0]);
}
sub do_cmd_butilityman {
    $mybuild = buildmanualname("Utility Programs manual") ;
    join('',$mybuild,$_[0]);
}
#
#sub do_cmd_bproblemsman {
#    &do_cmd_bproblemman() ;
#}
sub do_cmd_bproblemman {
    $mybuild = buildmanualname("Problems Resolution guide") ;
    join('',$mybuild,$_[0]);
}
sub do_cmd_bmiscman {
    $mybuild = buildmanualname("Miscellaneous guide") ;
    join('',$mybuild,$_[0]);
}

sub do_cmd_bregistered {
    join('',"&reg;",$_[0]);
}
sub do_cmd_bstar {
    join('',"&starf;",$_[0]);
}
sub do_cmd_vb {
    join('',"&#124;",$_[0]);
}
sub url {
    local($burl) = @_ ;
    join('','<a href="',$burl,'">',$burl,'</a>') ;
}
sub do_cmd_bcopyright {
    join('',"&copy;",$_[0]);
}
sub do_cmd_byear {
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) ;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
    $year += 1900 ;
    join('',$year,$_[0]);
}
sub do_cmd_btitlelogo {
    print "PCT: ALERT! do_cmd_btitlelogo: Is this used ?\n" ;
    join('',
	 # Logo and documentation title
	 '<div class="bsys_topclass" id="bsys_topid">',
	 '<img alt="BaculaSystems Logo" id="bsys_logo" src="blogo-header.png" />',
	 '<img alt="BaculaSystems Documentation text" id="bsys_doctitle" src="btitle-header.png" />',
	 '</div>',
	 # Breadcrumbs and search field
	 '<div class="bsys_breadnsearchclass" id="bsys_breadnsearchid">',
	 '<div class="bsys_searchclass" id="bsys_searchid">',
	 '<span class="bsys_searchtitleclass" id="bsys_searchtitleid">Search</span>',
	 '<input class="bsys_searchfieldclass" id="bsys_searchfieldid" type="text" value="Type your text here" />',
	 '</div>',
	 '<div class="bsys_breadcrumbsclass" id="bsys_breadcrumbsid">',
	 '<p class="bsys_breadcrumbscontentclass" id="bsys_breadcrumbscontentid">Main</p>',
	 '</div>',
	 '</div>',
	 $_[0]) ;
}

sub do_cmd_bru{
    join('','<span class="btool">bru</span>',$_[0]) ;
}

sub do_cmd_bdump {
    join('','<span class="btool">dump</span>',$_[0]) ;
}
sub do_cmd_tar {
    join('','<span class="btool">tar</span>',$_[0]) ;
}

sub do_cmd_mtx {
    join('','<span class="btool">mtx</span>',$_[0]) ;
}

sub do_cmd_mtxchanger {
    join('','<span class="btool">mtx-changer</span>',$_[0]) ;
}

sub do_cmd_bat {
    join('',eval("&bacs('BAT')"),$_[0]) ;
}
#
# Acronyms
sub bacs {
    local($bkey)=@_ ;
    print "PCT: bacs: key: >$bkey< @_\n" if ($VERBOSITY > 20) ;
    join('','<a class="bacronyms" href="Acronyms.html#', eval("&make_acronym_id(\$bkey)"),'">',$bkey,"</a>") ;
}
sub do_cmd_acs {
    local($_)=@_;
    &styled_text_chunk('','','','','','bacs',$_[0]) ;
}
sub bacsp {
    local($bkey) = @_ ;
    join('','<a class="bacronyms" href="Acronyms.html#',eval("&make_acronym_id($bkey)"),'">',$bkey,'s</a>') ; #,$_[0]) ;
}
sub do_cmd_acsp {
    local($_)=@_;
    my $bkey = &bnext_arg();
    &styled_text_chunk('','','','','','bacsp',$_[0]) ;
}
sub bac {
    local($bkey)=@_;
    my $bvalue = $bkey ;
    if ($bacronyms{$bkey}{'displayed'} == 0) {
	print "PCT: bac: never displayed: $bkey\n" if ($VERBOSITY > 10) ;
	$bacronyms{$bkey}{'displayed'} = 1 ;
	$bvalue = join('',$bacronyms{$bkey}{'value'},' (',$bkey,')') ;
    }
    join('','<a class="bacronyms" href="Acronyms.html#', eval("&make_acronym_id(\$bkey)"), '">', $bvalue, '</a>') ;
}
sub do_cmd_ac {
    local($_)=@_;
    my $bkey = &bnext_arg();
    &styled_text_chunk('','','','','','bac',@_) ;
}
sub bacp {
    local($bkey)=@_;
    my $bvalue = join('',$bkey,'s') ;
    if ($bacronyms{$bkey}{'displayed'} == 0) {
	print "PCT: do_cmd_acp: never displayed: $bkey\n" if ($VERBOSITY > 10) ;
	$bacronyms{$bkey}{'displayed'} = 1 ;
	$bvalue = join('',$bacronyms{$bkey}{'value'},'s (', $bkey,'s)') ;
    }
    join('','<a class="bacronyms" href="Acronyms.html#',eval("&make_acronym_id(\$bkey)"),'">', $bvalue, '</a>') ;
}
sub do_cmd_acp {
    local($_)=@_;
    &styled_text_chunk('','','','','','bacp',@_) ;
}

sub bacl {
    local($bkey)=@_;
    print "PCT: do_cmd_acl: key: $bkey\n" if ($VERBOSITY > 5) ;
    my $bvalue = $bacronyms{$bkey}{'value'} ;
    print "PCT: do_cmd_acl: val: $bvalue\n" if ($VERBOSITY > 5) ;
    join('','<a class="bacronyms" href="Acronyms.html#', eval("&make_acronym_id(\$bkey)"), '">', $bvalue, '</a>') ;
}
sub do_cmd_acl {
    local($_)=@_;
    my $bkey = &bnext_arg();
    &styled_text_chunk('','','','','','bacl',@_) ;
}

sub baclp {
    local($bkey)=@_;
    my $bvalue = $bacronyms{$bkey}{'value'} ;
    join('','<a class="bacronyms" href="Acronyms.html#', eval("&make_acronym_id(\$bkey)"),'">',$bvalue,'s</a>') ;
}
sub do_cmd_aclp {
    local($_)=@_;
    &styled_text_chunk('','','','','','baclp',@_) ;
}
sub make_acronym_id {
    local($bkey) = @_ ;
    $bacronyms{$bkey}{'seen'} = 1 ;
    print "PCT: make_acronym_id: $bkey seen\n" if ($VERBOSITY > 10) ;
    join('','bacronyms_',lc($bkey)) ;
}
#
# Images
sub bcompute_image {
    local($bclass,$bfilename,$blabel,$bcaption,$bsize) = @_ ;
    &anchor_label($blabel,$CURRENT_FILE,$_);    
    join('','<div class="',$bclass,'">',"\n",
	 '<a name="',$blabel,'">',"\n",
	 '<img class="',$bclass,'" src="',$bfilename,'" ',$bsize,'>',"\n",
	 '<div class="caption">',"\n",
	 '<span class="',$bclass,'">',$bcaption,'</span>',"\n",
	 '</div>',"\n",
	 '</a>',"\n",
	 '</div>',$_) ;
}
# Horizontal image
sub do_cmd_bimageH {
    local($_) = @_ ;
    # bimageH[an optional width]{image_filename}{caption to display}{label to use}
    print "do_cmd_bimageH: $_\n" if ($VERBOSITY > 20) ;
    local($bwidth,$bfilename,$bcaption,$blabel) ;
    $bwidth = &get_next_optional_argument() ;
    $bfilename = join('','../../images/',&bnext_arg(),'.png') ;
    $bcaption = &bnext_arg() ;
    $blabel = &bnext_arg() ;
    print "do_cmd_bimageH: Width = $bwidth\tFile = $bfilename\tCaption = $bcaption\tLabel = $blabel\n" if ($VERBOSITY > 100) ;
    if ($bwidth ne "") {
	print "do_cmd_bimageH: bwidth: $bwidth\n" if ($VERBOSITY > 200) ;
	if ($bwidth =~ /\[(.*)\\linewidth\]/) {
	    print "do_cmd_bimageH: ratio = $1\n" if ($VERBOSITY > 100) ;
	    $bwidth = $1 * 100 ;
	    $bwidth = join('','style="width: ',$bwidth,'%;"') ;
	}
    }
    print "do_cmd_bimageH: bwidth: $bwidth\n" if ($VERBOSITY > 10) ;
    &bcompute_image("bimageH",$bfilename,$blabel,$bcaption,$bwidth) ;
}
#
# Images with no orientation
sub do_cmd_bimageN {
    local($_) = @_ ;
    # bimageN[an optional width]{image_filename}{caption to display}{label to use}
    print "do_cmd_bimageN: $_\n" if ($VERBOSITY > 20) ;
    local($bwidth,$bfilename,$bcaption,$blabel) ;
    $bwidth = &get_next_optional_argument() ;
    $bfilename = join('','../../images/',&bnext_arg(),'.png') ;
    $bcaption = &bnext_arg() ;
    $blabel = &bnext_arg() ;
    print "do_cmd_bimageN: Width = $bwidth\tFile = $bfilename\tCaption = $bcaption\tLabel = $blabel\n" if ($VERBOSITY > 10) ;
    if ($bwidth ne "") {
	print "do_cmd_bimageN: bwidth: $bwidth\n" if ($VERBOSITY > 10) ;
	if ($bwidth =~ /\[(.*)\\linewidth\]/) {
	    $bwidth = join('','style="width: ',$1 * 100,'%;"') ;
	}
    }
    print "do_cmd_bimageN: bwidth: $bwidth\n" if ($VERBOSITY > 10) ;
    &bcompute_image("bimageN",$bfilename,$blabel,$bcaption,$bwidth) ;
}

#
# Images with no orientation
sub do_cmd_bimageV {
    local($_) = @_ ;
    # bimageV[an optional height]{image_filename}{caption to display}{label to use}
    print "do_cmd_bimageV: $_\n" if ($VERBOSITY > 20) ;
    local($bheight,$bfilename,$bcaption,$blabel) ;
    $bheight = &get_next_optional_argument() ;
    $bfilename = join('','../../images/',&bnext_arg(),'.png') ;
    $bcaption = &bnext_arg() ;
    $blabel = &bnext_arg() ;
    print "do_cmd_bimageV: Height = $bheight\tFile = $bfilename\tCaption = $bcaption\tLabel = $blabel\n" if ($VERBOSITY > 10) ;
    #
    # As we are in HTML, we can not calculate anything based on "linewidth". So either an absolute dimension is specified
    # and everything is fine
    # or no dimension or a relative dimension is provided and we are in trouble => no width no height.
    if ($bheight ne "" and $bheight !~ /\\/) {
	$bheight = join('','style: "height: ',$bheight,';"') ;
    }
    else {
	$bheight = "" ;
    }
    print "do_cmd_bimageV: bheight: $bheight\n" if ($VERBOSITY > 10) ;
    &bcompute_image("bimageV",$bfilename,$blabel,$bcaption,$bheight) ;
}

sub do_cmd_textcolor {
    local($_) = @_ ;
    my $bcolor = &bnext_arg() ;
    my $btext = &bnext_arg() ;
    print "PCT: do_cmd_textcolor: color: $bcolor - text: $btext\n" if ($VERBOSITY > 10) ;
    join('',$btext,$_) ;
}

sub do_cmd_vref {
    local($_) = @_ ;
    #
    # The only arg is the label name
    my $blabel=&bnext_arg() ;
    print "PCT: do_cmd_vref: label: $blabel\n" if ($VERBOSITY > 10) ;
    if (my $bfilename = $ref_files{$blabel}) {
	if ($bfilename ne $CURRENT_FILE) {
	    $burl = join('',$bfilename,'#',$blabel) ;
	}
	else {
	    $burl = join('','#',$blabel) ;
	}
    }
    else {
	$burl = join('','__TO_BE_REPLACED_WITH_HTML_FILENAME__','#',$blabel) ;
    }
    print "PCT: do_cmd_vref: ref_files: " . $ref_files{$blabel} . "\n\turl: $burl\n\tlabel: $blabel\n\tcaption: " .$captions{$blabel}."\n\tfilename: $bfilename\n\n" if ($VERBOSITY > 10) ;
    join('','(<a class="bref" href="',$burl,'" alt="',$captions{$blabel},'" >here</a>)',$_) ;
}

sub do_cmd_bref {
    local($_) = @_ ;
    my $burl = &bnext_arg();
    my $btext=&bnext_arg() ;
    print "\nPCT: do_cmd_bref: text: $btext - url: $burl\n" if ($VERBOSITY > 2) ;
    if ($burl !~ /http(s?):/) {
	if (my $bfilename = $ref_files{$burl}) {
	    if ($bfilename ne $CURRENT_FILE) {
		$burl = join('',$bfilename,'#',$burl) ;
	    }
	    else {
		$burl = join('','#',$burl) ;
	    }
	}
    }
    join('','<a class="bref" href="',$burl,'">',$btext,'</a>',$_) ;
}

sub do_cmd_bfootref {
    local($_) = @_ ;
    &do_cmd_bref($_) ;
}

sub do_cmd_textrightarrow {
    join('',"&#8594;",$_[0]);
}

sub do_cmd_bilink {
     local($_) = @_;
     local($btext) ;
     local($blink) = &get_next_optional_argument() ;
     $btext = &missing_braces unless
 	((s/$next_pair_pr_rx/$btext = $2; ''/eo)
	 ||(s/$next_pair_rx/$btext = $2; ''/eo));
     print "PCT: bilink: btext: $btext --- blink: $blink\n" if ($VERBOSITY > 3) ;
     &bprocess_ref("bilink",$btext) ;
}
sub do_cmd_bxdlink {
    # \bxdlink}[4]{\href{../#3/#3}{\textbf{#1}} (#4 \vref{#3-#2})}
    # Format is bxdlink{Mail Command in the Messages Resource}{blb:mailcommand}{main}{chapter}
    local($_) = @_ ;
    local($btext,$blabel,$bmanual,$bwhat,$text) ;
    $btext = &bnext_arg() ;
    $blabel = &bnext_arg() ;
    $bmanual = &bnext_arg() ;
    $bwhat = &bnext_arg() ;
    #
    # There is only one case we know where we are
    my $blinkedfile = "__TO_BE_REPLACED_WITH_HTML_FILENAME__" ;
    if ($bmanual eq $FILE) {
	$blinkedfile = $CURRENT_FILE ;
    }
    join('','<a class="bxdlink" href="../',$bmanual,'/',$blinkedfile,'#',$blabel,'">',$btext,'</a>',$_) ;
}


sub do_cmd_bvalueddirective {
    local($_)=@_;
    my $bdirectivename = &bnext_arg();
    my $bvalue = &bnext_arg();
    join('','<span class="bdirectivename">',$bdirectivename,'</span>',' = ','<span class="bvalue">',$bvalue,'</span>',$_) ;
}
				
sub do_cmd_bxlink {
    # Format is bxlink{Mail Command in the Messages Resource}{blb:mailcommand}{main}{chapter}
    local($_) = @_ ;
    local($btext,$blabel,$bmanual,$bwhat,$text) ;
    $btext = &bnext_arg() ;
    $blabel = &bnext_arg() ;
    $bmanual = &bnext_arg() ;
    $bwhat = &bnext_arg() ;
    #
    # There is only one case we know where we are
    my $blinkedfile = "__TO_BE_REPLACED_WITH_HTML_FILENAME__" ;
    if ($bmanual eq $FILE) {
	$blinkedfile = $CURRENT_FILE ;
    }
    join('','<a class="bxlink" href="../',$bmanual,'/',$blinkedfile,'#',$blabel,'">',$btext,' ',$bwhat,'</a>',$_) ;
}

sub do_cmd_label {
    local($_) = @_;
    local($label);
    print "do_cmd_label: converting $FILE to $CURRENT_FILE\n" if ($VERBOSITY > 5) ;
    $label = &missing_braces unless (
	(s/$next_pair_pr_rx\n?/$label = $2;''/eo)
	||(s/$next_pair_rx\n?/$label = $2;''/eo));
    &anchor_label($label,$CURRENT_FILE,$_);
    $label =~ s/<[^>]*>//go;
    $label =~ s/$label_rx/_/g;
    if ($ref_files{$label} ne $CURRENT_FILE) {
        $ref_files{$label} = $CURRENT_FILE;
        $noresave{$label} = 0; $changed = 1;
	print "PCT: do_cmd_label: ref_files: ". $ref_files{$label} . "\n" if ($VERBOSITY > 10) ;
    }
    print "<LABEL: $label>\n\t ref_files: ". $ref_files{$label} . "\n" if ($VERBOSITY > 3);
    join('',"<A class=\"blabel\" NAME=\"$label\">$anchor_mark</A>",$_);
}

# This is used by external style files ...
sub bprocess_ref {
    local($class, $btext) = @_;
    $btext = &balance_inner_tags($btext)
	if $btext =~ (/<\/([A-Z]+)>($math_verbatim_rx.*)<\1>/);
    $btext = &translate_environments($btext);
    $btext = &simplify(&translate_commands($btext))
	if ($btext =~ /\\/ );
    local($breference,$id);
    local($pretag) = &get_next_optional_argument;
    $pretag = &translate_commands($pretag) if ($pretag =~ /\\/);
    $breference = &missing_braces unless (
	(s/$next_pair_pr_rx/($id, $breference) = ($1, $2);''/eo)
	||(s/$next_pair_rx/($id, $breference) = ($1, $2);''/eo));
    if ($breference) {
	$breference =~ s/<[^>]*>//go ; #RRM: Remove any HTML tags
	$breference =~ s/$label_rx/_/g;	# replace non alphanumeric characters

	$symbolic_labels{"$pretag$breference$id"} = $btext if ($btext);
	if (($symbolic_labels{$pretag.$breference})&&!($btext)) {
	    $btext = $symbolic_labels{$pretag.$breference}
	}
	$btext = ( (!$btext && $breference) || $btext);

	print "\nLINK: Reference: $breference --- btext: $btext\n" if ($VERBOSITY > 3);
	# The quotes around the HREF are inserted later
	my $localfile = "" ;
	if ($ref_files{$breference}) {
	    $localfile = $ref_files{$breference} ;
	}
	join('','<a class="',$class,'" href=',$localfile,'#',$breference,'>',$btext,'</a>', $_);
    }
    else {
	print "Cannot find label argument after <$last_word>\n" if $last_word;
	$after_label . $_;
    }
}
#
# Quotes
sub do_cmd_bog {
    join('','<span class="bleftquote">','_left_quote_','</span>',$_[0]) ;
}
sub do_cmd_cog {
    join('','<span class="brightquote">','_right_quote_','</span>',$_[0]) ;
}

sub do_cmd_babove {
    join('','&ge;',$_[0]) ;
}

sub do_cmd_bwebdoc {
    local($_) = @_;
    local($label);
    $label = &missing_braces unless (
	(s/$next_pair_pr_rx\n?/$label = $2;''/eo)
	||(s/$next_pair_rx\n?/$label = $2;''/eo));
    &anchor_label($label,$CURRENT_FILE,$_);
}

sub do_cmd_bmathequation {
    local($_) = @_;
    local($bequation);
    $bequation = &missing_braces unless (
	(s/$next_pair_pr_rx\n?/$bequation = $2;''/eo)
	||(s/$next_pair_rx\n?/$bequation = $2;''/eo));
    $bequation =~ s/_([^\s]+)/<sub>\1<\/sub>/g ;
    $bequation =~ s/\n//g ;
    $bequation = join("\n",'__bbmathequation__',$bequation,'__bemathequation__') ;
    print "PCT: bmathequation: equation: $bequation\n" if ($VERBOSITY > 10) ;
    join('',$bequation,$_) ;
}

sub do_cmd_leqslant {
    join('',' &le; ',$_[0]);
}


# ======================================================================
1;
