# bacronyms.perl
#    by Philippe Chauvat <philippe@chauvat.eu>
#
# These procedures intend to provide a similar behavior than the LaTeX' one
# => displaying acronyms at the end of the document.
# =========================================================================
#
# Handling acronym environment
sub do_env_acronym {
    local($_) = @_;
    print "PCT: do_env_acronym\n" if ($VERBOSITY > 10) ;
    my @keys = sort { $a cmp $b } keys %bacronyms;
    foreach $bkey ( @keys ) {
	print "PCT: do_env_acronym: Key = $bkey - Value = " . $bacronyms{$bkey}{'value'} . "\n" if ($VERBOSITY > 100) ;
    }

    print "PCT: do_env_acronym $_\n" if ($VERBOSITY > 50) ;
    my $bcmd, $bpreviouskey = "" ;
    while (s/(?:[\s\n$comment_mark?]*)\\(\w+)/$bcmd=$1;''/e) {
	eval ("\$bpreviouskey = &do_cmd_$bcmd(\$bpreviouskey)") ;
	print "PCT: do_env_acronym: previous = $bpreviouskey\n" if ($VERBOSITY > 100) ;
    }
    @keys = sort { $a cmp $b } keys %bacronyms;
    my $currentletter = "_" ;
    my $result = "\n<div class=\"bacronymindex\">" ;
    my $closetag = "" ;
    foreach $bkey ( @keys ) {
	print "PCT: do_env_acronym: key = $bkey / value = " . $bacronyms{$bkey}{'value'} . " / seen = " . $bacronyms{$bkey}{'seen'} . "\n" if ($VERBOSITY > 100) ;
	if (defined $bacronyms{$bkey}{'seen'}) {
	    print "PCT: do_env_acronym: bkey = $bkey / fc = " . substr($bkey,0,1) . " / current letter = $currentletter\n"  if ($VERBOSITY > 50) ;
	    if ($bkey !~ /^$currentletter/) {
		$currentletter = substr($bkey,0,1) ;
		$result .= $closetag . "\n<p class=\"bacronymsindex\">$currentletter</p>\n" ;
		$closetag = "<!-- acronyms begining by " . $currentletter . " -->\n" ;
	    }
	    local $bid = &make_acronym_id($bkey) ;
	    $result .= "<p class=\"bacronymslist\" id=\"" . $bid . "\"><a name=\"" . $bid . "\">" . $bkey . "</a>: " . $bacronyms{$bkey}{'value'} . "</p>\n" ;
	}
    }
    $result .= "</div>\n" ;
    print "PCT: do_env_acronym: result: $result\n" if ($VERBOSITY > 10) ;
    $result ;
}


# enclose the contents of any user-defined labels within a group,
# else any style-change commands may bleed outside the label.
sub protect_acros {
    local($_) = @_ ;
    $_[0] ;
}


sub do_cmd_acro {
    # bdummy is just there to fit with the definition of do_cmd_acroextra
    local($bdummy)=@_;
    print "PCT: do_cmd_acro : previous key = $bdummy\n" if ($VERBOSITY > 100) ;
    my $bkey = bnext_arg();
    print "\nPCT: do_cmd_acro: bacronyms: key = $bkey\n"  if ($VERBOSITY > 50) ;
    my $bvalue = bnext_arg() ;
    print "bacronyms: value = $bvalue\n"  if ($VERBOSITY > 50) ;
    $bacronyms{$bkey}{'value'} = $bvalue ;
    $bacronyms{$bkey}{'displayed'} = 0 ;
    $bkey ;
}

sub do_cmd_acroextra {
    local($bkey) = @_ ;
    print "PCT: do_cmd_acroextra: taking care of more information for $bkey (" . $bacronyms{$bkey} . ")\n" if ($VERBOSITY > 50) ;
    my $bvalue = bnext_arg() ;
    print "PCT: do_cmd_acroextra: \"" . $bvalue . " to add...\n" if ($VERBOSITY > 50) ;
    $bacronyms{$bkey}{'value'} .= "<br/>" . $bvalue ;
    print "PCT: do_cmd_acroextra: $bkey / " . $bacronyms{$bkey} ."\n"  if ($VERBOSITY > 50) ;
    $bkey ;
}    
# ====================================================================== 
1;
