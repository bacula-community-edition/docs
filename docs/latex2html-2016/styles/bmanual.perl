# bmanual.perl by Philippe Chauvat <philippe@chauvaT.eu> 11-jan-2017
# All of this was in the book.perl file written by Ross Moore <ross@mpce.mq.edu.au>  09-14-97
#
# Extension to LaTeX2HTML V97.1 to support the "bmanual" document class
# and standard LaTeX2e class options.
#
# Change Log:
# ===========

package main;
do_require_package('bdefinitions');
do_require_package('graphicx');
do_require_package('bedition');
do_require_package('bcoverpage');
do_require_package('bacronyms') ;

%other_environments = (%other_environments, "bVerbatim") ;
# Suppress option-warning messages:

sub do_bmanual_a4paper{}
#
# Added by Philippe Chauvat
sub do_bmanual_bpaper{}
#
sub do_bmanual_a5paper{}
sub do_bmanual_b5paper{}
sub do_bmanual_legalpaper{}
sub do_bmanual_letterpaper{}
sub do_bmanual_executivepaper{}
sub do_bmanual_landscape{}
sub do_bmanual_final{}
sub do_bmanual_draft{}
sub do_bmanual_oneside{}
sub do_bmanual_twoside{}
sub do_bmanual_openright{}
sub do_bmanual_openany{}
sub do_bmanual_onecolumn{}
sub do_bmanual_twocolumn{}
sub do_bmanual_notitlepage{}
sub do_bmanual_titlepage{}
sub do_bmanual_openbib{}

sub do_bmanual_10pt{ $LATEX_FONT_SIZE = '10pt' unless $LATEX_FONT_SIZE; }
sub do_bmanual_11pt{ $LATEX_FONT_SIZE = '11pt' unless $LATEX_FONT_SIZE; }
sub do_bmanual_12pt{ $LATEX_FONT_SIZE = '12pt' unless $LATEX_FONT_SIZE; }

sub do_bmanual_leqno{ $EQN_TAGS = 'L'; }
sub do_bmanual_reqno{ $EQN_TAGS = 'R'; }
sub do_bmanual_fleqn{ $FLUSH_EQN = 1; }

sub do_cmd_thepart {
    join('', &do_cmd_Roman("${O}0${C}part${O}0$C"), ".", @_[0]) }

sub do_cmd_thechapter {
    join('', &do_cmd_arabic("${O}0${C}chapter${O}0$C"), ".", @_[0]) }


sub do_cmd_thesection {
    join('',&translate_commands("\\thechapter")
	, &do_cmd_arabic("${O}0${C}section${O}0$C"), @_[0]) }

sub do_cmd_thesubsection {
    join('',&translate_commands("\\thesection")
	,"." , &do_cmd_arabic("${O}0${C}subsection${O}0$C"), @_[0]) }

sub do_cmd_thesubsubsection {
    join('',&translate_commands("\\thesubsection")
	,"." , &do_cmd_arabic("${O}0${C}subsubsection${O}0$C"), @_[0]) }

sub do_cmd_theparagraph {
    join('',&translate_commands("\\thesubsubsection")
	,"." , &do_cmd_arabic("${O}0${C}paragraph${O}0$C"), @_[0]) }

sub do_cmd_thesubparagraph {
    join('',&translate_commands("\\theparagraph")
	,"." , &do_cmd_arabic("${O}0${C}subparagraph${O}0$C"), @_[0]) }


&addto_dependents('chapter','equation');
&addto_dependents('chapter','footnote');
&addto_dependents('chapter','figure');
&addto_dependents('chapter','table');

sub do_cmd_theequation {
    local($chap) =  &translate_commands("\\thechapter");
    join('', (($chap =~ /^(0\.)?$/)? '' : $chap)
        , &do_cmd_arabic("${O}0${C}equation${O}0$C"), @_[0]) }

sub do_cmd_thefootnote {
    local($chap) =  &translate_commands("\\thechapter");
    join('', (($chap =~ /^(0\.)?$/)? '' : $chap)
        , &do_cmd_arabic("${O}0${C}footnote${O}0$C"), @_[0]) }

sub do_cmd_thefigure {
    local($chap) =  &translate_commands("\\thechapter");
    join('', (($chap =~ /^(0\.)?$/)? '' : $chap)
        , &do_cmd_arabic("${O}0${C}figure${O}0$C"), @_[0]) }

sub do_cmd_thetable {
    local($chap) =  &translate_commands("\\thechapter");
    join('', (($chap =~ /^(0\.)?$/)? '' : $chap)
        , &do_cmd_arabic("${O}0${C}table${O}0$C"), @_[0]) }



# The following are copy-pasted from latex2html file.
# with huge simplifications...
sub do_env_bitemize {
    local($_) = @_;
    my $class = "bitemize" ;
    $itemize_level++;
    #RRM - catch nested lists
    &protect_useritems($_);
    $_ = &translate_environments($_);
    $class .= $itemize_level ;
    $itemize_level--;
    &blist_helper($_,'UL',$class);
}

# The following are copy-pasted from latex2html file.
# with huge simplifications...
sub do_env_bdescription {
    local($_) = @_;
    my $class = "bdescription" ;
    $itemize_level++;
    #RRM - catch nested lists
    print "PCT: do_env_bdescription: pour $_\n" if ($VERBOSITY > 50) ;
    &protect_useritems($_);
    $_ = &translate_environments($_);
    $class .= $itemize_level ;
    $itemize_level--;
    &blist_helper($_,'DL',$class);
}

# The following are copy-pasted from latex2html file.
# with huge simplifications...
sub do_env_benumerate {
    local($_) = @_;
    local $bstorecounter = $global{'enumi'} ;
    print "PCT: do_env_enumerate: bstorecounter: $bstorecounter\n" if ($VERBOSITY > 10) ;
    local $addtoclass = "" ;
    $global{'enumi'} = 0 ;
    my $class = "benumerate" ;
    ++$enum_level;
    # enclose contents of user-defined labels within a group,
    # in case of style-change commands, which could bleed outside the label.
    &protect_useritems($_);
    $_ = &translate_environments($_);	#catch nested lists
    $class .= $enum_level ;
    if ($global{'enumi'} < 0) {
	$addtoclass .= "m" ;
	$global{'enumi'} *= -1 ;
    }
    if ($global{'enumi'} > 0) {
	$class = join('',$class,$addtoclass,'shift',$global{'enumi'}) ;
    }
    print "PCT: do_env_enumerate: class: $class\n" if ($VERBOSITY > 10) ;
    $enum_result = &blist_helper($_, "OL", $class) ;
    --$enum_level;
    $global{'enumi'} = $bstorecounter ;
    $enum_result;
}


#
# Same as above: copy paste from latex2html
# Adapted to accept a class definition.
sub blist_helper {
    local($_, $tag, $class, $labels, $lengths) = @_;
    local($item_sep,$pre_items,$compact,$etag,$ctag);
    $ctag = $tag; $ctag =~ s/^(.*)\s.*$/$1/;

    # assume no styles initially for this list
    local($close_tags,$reopens) = &close_all_tags();
    local($open_tags_R) = [];
    local(@save_open_tags) = ();

    if (($tag =~ /DL/)&&$labels) {
	local($label,$aft,$br_id);
	s/\\item\b[\s\r]*([^\[])/do {
		$label = $labels; $aft = $1;
		$br_id = ++$global{'max_id'};
		$label = &translate_environments(
			"$O$br_id$C$label$O$br_id$C");
		join('',"\\item\[" , $label, "\]$aft" );
	    }/egm;
    }

    # This deals with \item[xxx] ...
    if ($tag =~ /DL/) {
	if ($labels && $lengths) { 
	    $item_sep = join('',"\n","</DD>","\n",'<DT class="',$class,'">') ;
	} else {
	    $item_sep = ($labels ? "<DT class=\"$class\">$labels\n" : '') ."</DT>\n<DD class=\"$class\">";
	}
	$etag = "</DD>\n";
	s/$item_description_rx[\r\s]*($labels_rx8)?[\r\s]*/"<DT>" . (($9)? "<A NAME=\"$9\">$1<\/A>" : $1 ) ."<\/DT>\n<DD class=\"$class\">"/em;
	print "PCT: blist_helper: DL: $1\n" if ($VERBOSITY > 10) ;
	s/$item_description_rx[\r\s]*($labels_rx8)?[\r\s]*/"<\/DD>\n<DT>" . (($9)? "<A NAME=\"$9\">$1<\/A>" : $1 ) ."<\/DT>\n<DD class=\"$class\">"/egm;
    } else {
	$item_sep = "\n</LI>\n<LI class=\"$class\">";
	$etag = "\n</LI>";
    }
    print "PCT: blist_helper: etag: $etag\n" if ($VERBOSITY > 100) ;
    # remove unwanted space before \item s
    s/[ \t]*\\item\b/\\item/gm;

    #JCL(jcl-del) - $delimiter_rx -> ^$letters
    print "PCT: blist_helper: sep: $item_sep --- " . $_ . "\n" if ($VERBOSITY > 200) ;
    s/\n?\\item\b[\r\s]*/$item_sep/egm;
    print "PCT: blist_helper: after end mark" . $_ ."\n" if ($VERBOSITY > 200) ;

    #RRM: cannot have anything before the first <LI>
    local($savedRS) = $/; $/='';
    $_ =~ /\Q$item_sep\E|<DT>|<LI>/s;
    #RRM: ...try putting it before the list-open tag
    if ($`) {
	print "PCT: blist_helper: passing in antiquote form\n" if ($VERBOSITY > 200) ;
	$preitems = $`; $_ = $&.$';
	print "PCT: blist_helper: preitems: $preitems\n" if ($VERBOSITY > 200) ;
	$preitems =~ s/<P( [^>]*)?>//gm;
	print "PCT: blist_helper: preitems: $preitems\nclose_tags: $close_tags\n" if ($VERBOSITY > 200) ;
	$close_tags .= "\n".$preitems if $preitems;
	print "PCT: blist_helper: close_tags: $close_tags\n" if ($VERBOSITY > 200) ;

    }
    $_ =~ s/^\s*<\/[^>]+>\s*//s;

    # remove \n from end of the last item
    $_ =~ s/\n$//s;
    $/ = $savedRS;
    print "PCT: blist_helper: etag: $etag --- ctag: $ctag --- content $_\n" if ($VERBOSITY > 200) ;
    join('',$close_tags,"\n<$tag class=\"$class\">\n", $_ , "$etag\n</$ctag>" , $reopens);
}

sub oldblist_helper {
    local($_, $tag, $class, $labels, $lengths) = @_;
    local($item_sep,$pre_items,$compact,$etag,$ctag);
    $ctag = $tag; $ctag =~ s/^(.*)\s.*$/$1/;

    # assume no styles initially for this list
    local($close_tags,$reopens) = &close_all_tags();
    local($open_tags_R) = [];
    local(@save_open_tags) = ();
    $item_sep = "\n</LI>\n<LI class=\"" . $class . "\">";
    $etag = "\n</LI>";

    # remove unwanted space before \item s
    s/[ \t]*\\item\b/\\item/gm;

    #JCL(jcl-del) - $delimiter_rx -> ^$letters
    s/\n?\\item\b[\r\s]*/$item_sep/egm;

    #RRM: cannot have anything before the first <LI>
    local($savedRS) = $/; $/='';
    $_ =~ /\Q$item_sep\E|<DT>|<LI>/s;
    #RRM: ...try putting it before the list-open tag
    if ($`) { 
	$preitems = $`; $_ = $&.$';
	$preitems =~ s/<P( [^>]*)?>//gm;
	$close_tags .= "\n".$preitems if $preitems;
    }
    $_ =~ s/^\s*<\/[^>]+>\s*//s;

    # remove \n from end of the last item
    $_ =~ s/\n$//s;
    $/ = $savedRS;

    join('',$close_tags,"\n<$tag class=\"$class\">\n", $_ , "$etag\n</$ctag>" , $reopens);
}

sub do_cmd_title {
    local($_) = @_;
    &get_next_optional_argument;
    local($making_title,$next) = (1,'');
    $next = &missing_braces unless (
	(s/$next_pair_pr_rx/$next = $2;''/eo)
	||(s/$next_pair_rx/$next = $2;''/eo));
    $t_title = &translate_environments($next);
    $t_title = &translate_commands($t_title);
    $toc_sec_title = &purify(&translate_commands($next));
    $TITLE = (($toc_sec_title)? $toc_sec_title : $default_title)
	unless ($TITLE && !($TITLE =~ /^($default_title|\Q$FILE\E)$/));
    $TITLE =~ s/<A[^>]*><SUP>\d+<\/SUP><\/A>/$1/g;
    $_;
}

sub do_cmd_url {
    local($_) = @_ ;
    my $result ;
    my $arg = &bnext_arg() ;
    $result = join('','<a class="burl" href="' . $arg . '">' . $arg . '</a>') ;
    print "PCT: do_cmd_url: arg: $arg - result: $result\n" if ($VERBOSITY > 10) ;
    return $result ;
}
    
sub do_cmd_texttt {
    local($_) = @_ ;
    my $arg = &bnext_arg() ;
    &styled_text_chunk('','','','','','btexttt',@_) ;
}

sub do_cmd_textit {
    local($_) = @_ ;
    my $arg = &bnext_arg() ;
    &styled_text_chunk('','','','','','btextit',@_) ;
}

sub do_cmd_textsl {
    local($_) = @_ ;
    my $arg = &bnext_arg() ;
    &styled_text_chunk('','','','','','btextsl',@_) ;
}

sub do_cmd_textbf {
    local($_) = @_ ;
    my $arg = &bnext_arg() ;
    &styled_text_chunk('','','','','','btextbf',@_) ;
}

sub do_cmd_emph {
    local($_) = @_ ;
    my $arg = &bnext_arg() ;
    &styled_text_chunk('','','','','','bemph',@_) ;
}

sub btextbf {
    local($btext) = @_ ;
    print "PCT: bbf: btext: $btext\n" if ($VERBOSITY > 10) ;
    join('','<span class="bbf">',$btext,'</span>') ;
}
sub btexttt {
    local($btext) = @_ ;
    print "PCT: btt: btext: $btext\n" if ($VERBOSITY > 10) ;
    join('','<span class="btt">',$btext,'</span>') ;
}
sub btextit {
    local($btext) = @_ ;
        print "PCT: bit: btext: $btext\n" if ($VERBOSITY > 10) ;
    join('','<span class="bit">',$btext,'</span>') ;
}
sub btextsl {
    local($btext) = @_ ;
        print "PCT: bsl: btext: $btext\n" if ($VERBOSITY > 10) ;
    join('','<span class="bsl">',$btext,'</span>') ;
}
sub bemph {
    local($btext) = @_ ;
    print "PCT: bemp: btext: $btext\n" if ($VERBOSITY > 10) ;
    join('','<span class="bemph">',$btext,'</span>') ;
}

sub do_acs {
    # acronym short version
    local($_) = @_ ;
    my $arg = &bnext_arg() ;
    &styled_text_chunk('SPAN','CLASS="bemph"','','','','',@_) ;
}

sub do_cmd_dominitoc {
    local($_) = @_ ;
    $_[0] ;
}
sub do_cmd_phantomsection {
    local($_) = @_ ;
    $_[0] ;
}
# Longtable management
sub bcompute_columns {
    local($_) = @_ ;
    my($nbcols,$bborder) = (0,'') ;
    my @cols ;
    print "PCT: compute_columns: columns: $_\n" if ($VERBOSITY > 10) ;
    s/\[.*?\]//g ;
    while ($_) {
	if (s/^\s*(\|)\s*//) {
	    $bborder = 'border' ;
	    print "PCT: compute_columns: got a border on column $nbcols\n" if ($VERBOSITY > 11) ;
	}
	if (s/^\s*(l|X|L)\s*//) {
	    push @cols,join('','btable',$bborder,'left') ;
	    $bborder = '' ;
	    print "PCT: compute_columns: got a left column\n" if ($VERBOSITY > 11) ;
	}
	if (s/^\s*(r|R)\s*//) {
	    push @cols,join('','btable',$bborder,'right') ;
	    $bborder = '' ;
	    print "PCT: compute_columns: got a right column\n" if ($VERBOSITY > 11) ;
	}
	if (s/^\s*(c|C)\s*//) {
	    push @cols,join('','btable',$bborder,'center') ;
	    $bborder = '' ;
	    print "PCT: compute_columns: got a center column\n" if ($VERBOSITY > 11) ;
	}
	if (s/^\s*p\s*//) {
	    push @cols,join('','btable',$bborder,'left') ;
	    $bborder = '' ;
	    my $bwidth = &bnext_arg() ;
	    print "PCT: compute_columns: got a p column width $bwidth\n" if ($VERBOSITY > 11) ;
	}
    }
    if ($bborder ne '') {
	print "PCT: bcompute_columns: add a last border\n" if ($VERBOSITY > 10) ;
	my $col = $cols[$#cols] ;
	print "PCT: bcompute_columns: before: $col\n" if ($VERBOSITY > 10) ;
	$col = join('',$col,$bborder) ;
	print "PCT: bcompute_columns: after: $col\n" if ($VERBOSITY > 10) ;
	$cols[$#cols] = $col ;
    }
    foreach my $c (@cols) {
	print "PCT: bcompute_columns: column: $c\n" if ($VERBOSITY >10) ;
    }
    @cols ;
}
sub bcompute_longtable_headers_footers {
#    local ($_) = @_ ;
    local(%bblocks) = () ;
    print "PCT: bcompute_longtable_headers_footers first $_\n" if ($VERBOSITY > 100) ;
    while (s/\s*(([\S\s]+?)\\end((first|)head|(last|)foot))/$bblocks{$3}=$2;''/e) {
	print "PCT: bcompute_longtable_headers_footers: running $_\n" if ($VERBOSITY > 100) ;
	print "PCT: bcompute_longtable_headers_footers: key: $3 -- bblock = $bblocks{$3}\n" if ($VERBOSITY > 100) ;
	
    }
    print "PCT: bcompute_longtable_headers_footers: last: $_\n" if ($VERBOSITY > 100) ;
    %bblocks ;
}
sub do_cmd_hline {
    local($_) = @_ ;
    return join('','<tr class="bhline">',"\n",$_[0]) ;
}
sub do_cmd_midrule {
    local($_) = @_ ;
    return join('','<tr class="midrule">',"\n",$_[0]) ;
}
sub do_cmd_toprule {
    local($_) = @_ ;
    return join('','<tr class="toprule">',"\n",$_[0]) ;
}
sub do_cmd_bottomrule {
    local($_) = @_ ;
    return join('','<tr class="bottomrule">',"\n",$_[0]) ;
}
sub do_cmd_bheader {
    local($_) = @_ ;
    $btext = &bnext_arg() ;
    return join('',$btext) ;
}
sub do_cmd_bsubheader {
    local($_) = @_ ;
    $btext = &bnext_arg() ;
    return join('',$btext) ;
}
sub do_cmd_bcontcaption{
}
sub do_cmd_multicolumn {
    local($_) = @_ ;
    my($bcols,$bformat,$bvalue,$next) ;
    print "PCT: do_cmd_multicolumn: content at entry: $_\n" if ($VERBOSITY > 10) ;
    s/$bbalise_rx(.*)$OP\1$CP$bbalise_rx(.*)$OP\3$CP$bbalise_rx($bdeferred_rx$OP\7$CP)?(.*)$OP\5$CP/$bcols=$2;$bformat=$4;$bvalue=$8;''/e ;
    print "PCT: do_cmd_multicolumn: cols: $bcols, format: $bformat, value: $bvalue\n" if ($VERBOSITY > 11) ;
    my @cols = &bcompute_columns($bformat) ;
    print "PCT: do_cmd_multicolumn: column type: " . $cols[0] . "\n" if ($VERBOSITY > 11) ;
    print "PCT: do_cmd_multicolumn: content: $_\n" if ($VERBOSITY > 10) ;
    return join('','<td class="',$cols[0],'"',(($bcols > 1) ? ' colspan="' . $bcols . '"' : ''),'>',$bvalue,'</td>',"\n",$_) ;
}
sub do_cmd_multirow {
    local($_) = @_ ;
    my $blines = &bnext_arg() ;
    my $bheight = &bnext_arg() ;
    my $bcontent = &bnext_arg() ;
    return join('','<td class="bmultirow">',$bcontent,'</td>',$_) ;
}
#
# When dealing with table headers
sub bcompute_multicolumn {
    local($_) = @_ ;
    my($bcols,$bformat,$bvalue) ;
    $bcols = &bnext_arg() ;
    $bformat = &bnext_arg() ;
    $bvalue = &bnext_arg() ;
    print "PCT: do_cmd_multicolumn: before compute columns\n" if ($VERBOSITY > 100) ;
    my @cols = &bcompute_columns($bformat) ;
    print "PCT: do_cmd_multicolumn: cols: $bcols, format: $bformat, value: $bvalue, col0: " . $cols[0] . "\n" if ($VERBOSITY > 110) ;
    print "PCT: do_cmd_multicolumn: content: $_\n" if ($VERBOSITY > 100) ;
    return join('','<td class="',$cols[0],'"',(($bcols > 1) ? ' colspan="' . $bcols . '"' : ''),'>',$bvalue,'</td>',"\n",$_) ;
}

sub bcompute_table_line {
    local($_,@cols) = @_ ;
    print "PCT: bcompute_table_line: content entry $_\n" if ($VERBOSITY > 10) ;
    s/[\s\n\r]*\\\\//gm ;
    @bcells = split /;SPMamp;/ ;
    my $bline ;
    my $i = 0 ;
    foreach my $c (@bcells) {
	print "PCT: bcompute_table_line: cell: $c\n" if ($VERBOSITY > 10) ;
	my $bcell = &translate_commands($c) ;
	print "PCT: bcompute_table_line: translated cell: $bcell\n" if ($VERBOSITY > 10) ;
	#
	# Do we already have an HTML cell?
	if ($bcell =~ /^\s*<t(d|r|h)(\s+|>)/) {
	    $bline = join('',$bline,$bcell) ;
	}
	# if not we build one
	else {
	    print "PCT: bcompute_table_line: i=$i - col: " . $cols[$i] . "\n" if ($VERBOSITY >  10) ;
	    $bline = join('',$bline,'<td class="',$cols[$i],'">',$bcell,'</td>',"\n") ;
	}
	$i++ ;
    }
    print "PCT: bcompute_table_line: bline: $bline\n" if ($VERBOSITY > 30) ;
    $bline ;
}
sub do_env_longtable {
    local($_) = @_;
    print "PCT: do_env_longtable: Content $_\n" if ($VERBOSITY > 10) ;
    s/[\n\r]//mg ;
    s/\\\\/\\\\\n/g ;
    s/\[.*?\]//g ;
    print "PCT: do_env_longtable: Content after filtering $_\n" if ($VERBOSITY > 10) ;
    local($halign, $anchors) = ('','');
    local($border, $attribs );
    local($cap_env,$captions) = ('longtable','') ;
    my $bcaption ;
    ($_,$anchors) = &extract_labels($_) ;
    print "PCT: do_env_longtable: labels: $anchors\n" if ($VERBOSITY > 10) ;
    my $btexlabel = $anchors ;
    $btexlabel =~ s/<a name="(.*?)">.*?<\/a>/$1/mi ;
    do {
 	local($contents) = $_ ;
 	$bcaption = &extract_captions($cap_env); $_ = $contents;
 	print "PCT: do_env_longtable: caption: $cap_env - captions: $captions - bcaption: $bcaption - mark: $caption_mark\n" if ($VERBOSITY > 10) ;
    } if (/\\caption/) ;
    print "PCT: do_env_longtable: Current content after labels and captions: $_\n" if ($VERBOSITY > 10) ;
        $_ = &translate_environments($_);
    #
    # Make some cleaning removing tex2html_deferred stuff.
    local @cols = &bcompute_columns(&bnext_arg()) ;
    local (%blocks) = &bcompute_longtable_headers_footers() ;
    my $blongtable = join("\n",'<div class="blongtable">','<table class="blongtable">');
    print "PCT: do_env_longtable: blongtable: $blongtable - 1\n" if ($VERBOSITY > 10) ;
    print "PCT: do_env_longtable: rest of content: $_\n" if ($VERBOSITY > 10) ;
    if ($bcaption) {
	my $bcaption_id ;
	$bcaption =~ s/$OP(\d+)$CP(.*)$OP\1$CP/$bcaption_id=$1;$2/e ; # extract the caption, nothing else, and get the id.
	print "PCT: bcaption: $bcaption - bcaption_id: $bcaption_id - 1: $1 - 2: $2\n" if ($VERBOSITY > 10) ;
	$captions{$btexlabel} = $bcaption ;
	if ($anchors) {
	    print "PCT: do_env_longtable: anchors: $anchors\n" if ($VERBOSITY > 10) ;
	    $anchors =~ s/$anchor_mark/$bcaption/e ;
	    $bcaption = $anchors ;
	}
	$bcaption = join('','<caption class="blongtable" id="',$bcaption_id,'">',$bcaption,'</caption>') ;# HTML code for caption.
	print "PCT: bcaption HTML: $bcaption\n" if ($VERBOSITY > 10) ;
	$blongtable = join("\n",$blongtable,$bcaption) ;
	print "PCT: do_env_longtable: blongtable: $blongtable - caption\n" if ($VERBOSITY > 10) ;
    }
    print "PCT: do_env_longtable: rest of content after caption: $_\n" if ($VERBOSITY > 10) ;
    #
    # We got the headers and footers
    my $bcloseline ='</tr>' ;
    my $bopenline = join('','<tr class="blongtable">',"\n") ;
    foreach my $b (keys %blocks) {
	# Only the first header is used because there is no "newpage" in HTML
	print "PCT: do_env_longtable: key: $b - data: " . $blocks{$b} . "\n" if ($VERBOSITY > 10) ;
	if ($b =~ /firsthead/) {
	    my $bfirsthead = $blocks{$b} ;
	    $bfirsthead =~ s/\\rowcolor$O(\d+)$C.*$O\1$C//gm ;
	    $bfirsthead =~ s/\\rowcolor$OP(\d+)$CP.*$OP\1$CP//gm ;
	    while (($bfirsthead =~ s/(^\s*\\(hline|(top|mid|bottom)rule))|([\s\S]*?\\\\)//m)) {
		my $bvalue ;
		print "PCT: do_env_longtable: line: $2 - $4\n" if ($VERBOSITY > 10) ;
		if ($2 ne '') {
		    $bopenline = join('','<tr class="b',$2,'">',"\n") ;
		}
		else {
		    print "PCT: do_env_longtable: defined cols: @cols\n" if ($VERBOSITY > 10) ;
		    $bvalue = &bcompute_table_line($4,@cols) ;
		    #
		    # As we are describing the header, replacing <TD> by <TH> and </TD> by </TH> 
		    $bvalue =~ s/<(\/)?td/<\1th/gmi ;
		    $blongtable = join('',$blongtable,"\n",$bopenline,$bvalue,$bcloseline,"\n") ;
		    $bopenline = '<tr class="blongtable">' ;
		}
		print "PCT: do_env_longtable: blongtable: $blongtable\n" if ($VERBOSITY > 10) ;
	    }
	    print "PCT: do_env_longtable: bfirsthead: $bfirsthead - blongtable: $blongtable - headers \n" if ($VERBOSITY > 10) ;
	}
    }
    print "PCT: do_env_longtable: blongtable: $blongtable - 3 \nAnd the rest of content after headers: $_\n" if ($VERBOSITY > 10) ;
    while (s/(^\s*\\(hline|(top|mid|bottom)rule))|([\s\S]*?\\\\)//m) {
	my $bvalue ;
	print "PCT: do_env_longtable: line: $2 - $4\n" if ($VERBOSITY > 10) ;
	if ($2 ne '') {
	    $bopenline = join('','<tr class="b',$2,'">',"\n") ;
	}
	else {
	    print "PCT: do_env_longtable: defined cols: @cols\n" if ($VERBOSITY > 10) ;
	    $bvalue = &bcompute_table_line($4,@cols) ;
	    $blongtable = join('',$blongtable,"\n",$bopenline,$bvalue,$bcloseline,"\n") ;
	    $bopenline = '<tr class="blongtable">' ;
	}
	print "PCT: do_env_longtable: blongtable: $blongtable\n" if ($VERBOSITY > 10) ;
    }
    $blongtable = join("\n",$blongtable,'</table>','</div>') ;
    $blongtable ;
}

sub do_cmd_rowcolor {
    local($_) = @_ ;
    my $bcolor = &bnext_arg() ;
    return $_ ;
}

sub do_cmd_footnote {
    local($_) = @_ ;
    my $bnote = &bnext_arg() ;
    return join('','<span class="bfootnote">','note','<span class="bfootnotetext">',$bnote,'</span>','</span>',' ',$_) ;
}

sub do_bincludegraphics {
  my($file,$options,$origcall)=@_;
  my $save = $_;
  my $result ;
  local $_;
  # --------------------------------------------------
  # Parse options
  my ($v,@bb,@vp,$clip,$trim,$rqw,$rqh,$rqs,$aspect,$a,$rotfirst, $punt);
  my $boptions="" ;
  foreach (split(',',$options)){
      /^\s*(\w+)(=\s*(.*))?\s*$/;  $_=$1; $v=$3;
      if($GRAPHICS_OPTHIDE{$_}){ next; } # Ignore this option
      if($GRAPHICS_OPTPUNT{$_}){ $punt=1; last; } # This one we can't handle.
      if   (/^width$/) {
	  if ($v =~ /\\linewidth/) {
	      $v =~ s/\\linewidth/100/g ;
	      $v = eval($v) ;
	      $v .= '%' ;
	  }
	  $boptions .= join('',' ','width=',$v) ;
      }
      elsif(/^height$/)   {
	  if ($v =~ /\\linewidth/) {
	      $v =~ s/\\linewidth/100/g ;
	      $v = eval($v) ;
	      $v .= '%' ;
	  }
	  $boptions .= join('',' ','height=',$v) ;
      }
      else { &write_warnings("Unrecognized option $_ to \\includegraphics"); }
  }
  print "PCT: do_includegraphics: width: $rqw - height: $rqs\n" if ($VERBOSITY > 10) ;
  join('',
       '<div class="includegraphics">',
       "\n",
       '<img class="includegraphics" src="',
       '../../images/',$file,'.png',
       '" ',$boptions,'>',
       "\n",
       '</div>',$save) ;
}
1;	# Must be last line


