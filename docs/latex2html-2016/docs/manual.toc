\contentsline {paragraph}{}{1}
\contentsline {paragraph}{History}{1}
\contentsline {section}{\numberline {1}Overview}{1}
\contentsline {subsection}{\numberline {1.1}List of Features}{2}
\contentsline {subsection}{\numberline {1.2}Exemplary Documents}{4}
\contentsline {section}{\numberline {2}Installation and Further Support}{6}
\contentsline {subsection}{\numberline {2.1}Requirements}{6}
\contentsline {subsection}{\numberline {2.2}Installation on Windows}{8}
\contentsline {subsection}{\numberline {2.3}Getting \latextohtml }{9}
\contentsline {subsection}{\numberline {2.4}Installing \latextohtml }{11}
\contentsline {subsection}{\numberline {2.5}Getting Support and More Information}{14}
\contentsline {section}{\numberline {3}Environments and Special Features}{15}
\contentsline {subsection}{\numberline {3.1}Variation with HTML Versions}{15}
\contentsline {subsection}{\numberline {3.2}Internationalisation}{16}
\contentsline {subsubsection}{\numberline {3.2.1}Alternate Font Encodings}{17}
\contentsline {subsubsection}{\numberline {3.2.2}Multi-lingual documents, using Images}{18}
\contentsline {subsection}{\numberline {3.3}Mathematics}{18}
\contentsline {subsection}{\numberline {3.4}Figures and Image Conversion}{22}
\contentsline {subsubsection}{\numberline {3.4.1}An Embedded Image Example}{25}
\contentsline {subsubsection}{\numberline {3.4.2}Image Sharing and Recycling}{26}
\contentsline {subsubsection}{\numberline {3.4.3}Quality of Printed Images}{26}
\contentsline {subsection}{\numberline {3.5}Figures, Tables and Arbitrary Images}{27}
\contentsline {subsection}{\numberline {3.6}Document Classes and Options}{29}
\contentsline {subsection}{\numberline {3.7}Packages and Style-Files}{29}
\contentsline {subsubsection}{\numberline {3.7.1}Fancy List-Markers}{31}
\contentsline {subsubsection}{\numberline {3.7.2}Support for \FoilTeX }{32}
\contentsline {subsubsection}{\numberline {3.7.3}Indicating Differences between Document Versions}{32}
\contentsline {subsection}{\numberline {3.8}Indexing}{33}
\contentsline {subsubsection}{\numberline {3.8.1}Integrated Glossary and Index}{34}
\contentsline {section}{\numberline {4}Hypertext Extensions to \LaTeX {}}{38}
\contentsline {subsection}{\numberline {4.1}Hyper-links in \LaTeX }{41}
\contentsline {subsection}{\numberline {4.2}Including Arbitrary HTML Mark-up and Comments}{42}
\contentsline {subsection}{\numberline {4.3}Arbitrary Tags and Attributes}{44}
\contentsline {subsection}{\numberline {4.4}Conditional Text}{46}
\contentsline {subsection}{\numberline {4.5}Symbolic References shown as Hyperized Text}{48}
\contentsline {subsection}{\numberline {4.6}Hypertext Links in Bibliographic References (Citations)}{50}
\contentsline {subsection}{\numberline {4.7}Symbolic References between Living Documents}{51}
\contentsline {subsubsection}{\numberline {4.7.1}Cross-Referencing Example}{53}
\contentsline {subsection}{\numberline {4.8}Miscellaneous commands for \texttt {HTML} effects}{53}
\contentsline {subsection}{\numberline {4.9}Active Image Maps}{56}
\contentsline {subsection}{\numberline {4.10}Document Segmentation\footnote {This feature is supported only for users of \LaTeXe .}}{57}
\contentsline {subsubsection}{\numberline {4.10.1}A Segmentation Example}{59}
\contentsline {section}{\numberline {5}Customising the Layout of HTML pages}{62}
\contentsline {subsection}{\numberline {5.1}Developing Documents using \textup {\LaTeX 2\texttt {HTML}}{}}{62}
\contentsline {subsection}{\numberline {5.2}Command-Line Options}{63}
\contentsline {subsubsection}{\numberline {5.2.1}Options controlling Titles, File-Names and Sectioning}{64}
\contentsline {subsubsection}{\numberline {5.2.2}Options controlling Extensions and Special Features}{66}
\contentsline {subsubsection}{\numberline {5.2.3}Switches controlling Image Generation}{69}
\contentsline {subsubsection}{\numberline {5.2.4}Switches controlling Navigation Panels}{70}
\contentsline {subsubsection}{\numberline {5.2.5}Switches for Linking to other documents}{71}
\contentsline {subsubsection}{\numberline {5.2.6}Switches for Help and Tracing}{72}
\contentsline {subsubsection}{\numberline {5.2.7}Other Configuration Variables, without switches}{73}
\contentsline {subsection}{\numberline {5.3}Extending the Translator}{78}
\contentsline {subsubsection}{\numberline {5.3.1}Asking the Translator to Ignore Commands}{78}
\contentsline {subsubsection}{\numberline {5.3.2}Asking the Translator to Pass Commands to \LaTeX }{79}
\contentsline {subsubsection}{\numberline {5.3.3}Handling ``order-sensitive'' Commands}{79}
\contentsline {subsection}{\numberline {5.4}Customising the Navigation Panels}{80}
\contentsline {section}{\numberline {6}Known Problems}{82}
\contentsline {subsection}{\numberline {6.1}Troubleshooting}{82}
\contentsline {section}{\numberline {}Glossary of variables and file-names}{90}
\contentsline {section}{\numberline {}Index}{100}
