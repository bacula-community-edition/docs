== Purpose

This directory contains Bacula Manual cover pages in an SVG format. As SVG is XML based, Bacula version and Date (as of 22nd of Oct. 2020) are specified as parameters:
__BVERSION__
__BDATE__

The process to build the covers in PDF is the following:
- SVG files are converted to .tpl while replacing __PARAMETER__ to their respective values
- TPL files are converted to .pdf through inkscape export features.

Inkscape is complaining when exporting a non .svg file therefore, both STDERR and STDOUT are sent to their .svg_error file

== Inkscape:
   Both 0.x version and 1.x version are handled by the Makefile.
